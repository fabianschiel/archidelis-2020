<?php

$sLangName = 'Magyar';

$aLang = array(
    'charset'                                       => 'utf-8',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_0'           => 'Functional',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_1'           => 'Marketing (data transmission to USA included)',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_2'           => 'Essential',
    'NET_COOKIE_MANAGER_POPUP_HEADLINE'             => 'Privacy settings',
    'NET_COOKIE_NOTE_CLOSE'                         => 'Accept',
    'NET_COOKIE_MANAGER_MORE_INFO'                  => 'more information',
    'NET_COOKIE_NOTE_ADJUST_SETTINGS'               => 'Settings',
    'NET_COOKIE_NOTE_HIDE_SETTINGS'                 => 'Back',
    'NET_COOKIE_NOTE_WELCOMETEXT'                   => 'We use cookies on our website to offer you the best possible user experience. Some of the cookies are essential< to ensure your input for the functionalities provided by us - in particular a successful purchase - (session cookies). Others help us to improve your user experience on this website. Please note that we also provide cookies from US companies - specifically Google Analytics - and that data may be transferred to a third country outside the EU. In particular, there is a risk that your data may be accessed by US authorities for control and monitoring purposes and that no effective legal remedies are available. If you do not want this, then only accept the essential cookies listed in the settings. You can revoke all your consents at any time with effect for the future.',
);
