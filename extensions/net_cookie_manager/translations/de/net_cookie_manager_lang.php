<?php

$sLangName = 'Deutsch';

$aLang = array(
    'charset'                                       => 'utf-8',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_0'           => 'Funktional',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_1'           => 'Marketing (inkl. Datenweitergabe in die USA)',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_2'           => 'Essentiell',
    'NET_COOKIE_MANAGER_POPUP_HEADLINE'             => 'Privatsphäre Einstellungen',
    'NET_COOKIE_NOTE_CLOSE'                         => 'Akzeptieren',
    'NET_COOKIE_MANAGER_MORE_INFO'                  => 'mehr Informationen',
    'NET_COOKIE_NOTE_ADJUST_SETTINGS'               => 'Einstellungen',
    'NET_COOKIE_NOTE_HIDE_SETTINGS'                 => 'Zur&uuml;ck',
    'NET_COOKIE_NOTE_WELCOMETEXT'                   => 'Wir nutzen Cookies auf unserer Website, um Ihnen eine bestmögliche Nutzererfahrung zu bieten. Einige der Cookies sind essenziell,< um Ihre Eingaben für die von uns bereitgestellten Funktionalitäten – insbesondere einen erfolgreichen Kaufabschluss – sicherzustellen (Session Cookies). Andere helfen uns, Ihre Nutzererfahrung mit dieser Website zu verbessern. Bitte beachten Sie, dass wir auch Cookies von US-amerikanischen Firmen zur Verfügung stellen – konkret Google Analytics – und dass damit eine Drittlandübermittlung von Daten außerhalb der EU stattfinden kann. Es besteht insbesondere das Risiko, dass Ihre Daten dem Zugriff durch US-Behörden zu Kontroll- und Überwachungszwecken unterliegen und dagegen keine wirksamen Rechtsbehelfe zur Verfügung stehen. Wünschen Sie dies nicht, dann akzeptieren Sie nur die in den Einstellungen angeführten essentiellen Cookies. Alle Ihre Einwilligungen können Sie jederzeit mit Wirkung für die Zukunft widerrufen.',
);
