<?php

$sLangName = 'Deutsch';

$aLang = array(
    'charset'                                       => 'utf-8',
    'NETENSIO_NAVIGATION'                           => 'Netensio Software',
    'netcookiemanager'                              => 'Cookie Manager',
    'cookiemanager'                                 => 'Cookie Manager',
    'cookiemanager_list'                            => 'Cookie Manager - Cookie Liste',
    'cookiemanager_main'                            => 'Cookie-Verwaltung',

    'NET_COOKIEMANAGER_LIST_MENUITEM'               => 'Cookie Manager',
    'NET_COOKIEMANAGER_LIST_MENUSUBITEM'            => 'Cookie Abfrage Verwaltung',

    'NET_COOKIE_MANAGER_MAIN_ACTIVE'                => 'Aktiv',
    'NET_COOKIE_MANAGER_MAIN_COOKIEID'              => 'Cookie ID',
    'NET_COOKIE_MANAGER_MAIN_TITLE'                 => 'Cookie Name',
    'NET_COOKIE_MANAGER_MAIN_DESC'                  => 'Beschreibung',
    'NET_COOKIE_MANAGER_MAIN_GROUP'                 => 'Cookie Kategorie',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_0'           => 'Funktional',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_1'           => 'Marketing',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_2'           => 'Essentiell',
    'NET_COOKIE_MANAGER_MAIN_SORT'                  => 'Sortierung',
    'NET_COOKIE_MANAGER_MAIN_SAVE'                  => 'Speichern',

    'NET_COOKIEMANAGER_NEWCOOKIE'                   => 'Neuen Cookie Eintrag anlegen',

    'NET_GENERAL_COOKIE_TITLE'                      => 'Bezeichnung des Cookies',
    'NET_GENERAL_COOKIE_GROUP'                      => 'Cookie Kategorie',

);
