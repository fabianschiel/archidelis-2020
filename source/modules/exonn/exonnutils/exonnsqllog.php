<?php


class exonnsqllog extends ADOConnection
{

    protected $_aTableNoLog = array(
        'oxamazon_action',
        'oxebay_action',
        'ebayorderserr',
        'oxcounters',
        'ebaycat_specifics',
        'ebay_category',
        'oxbankingprocess',
        'oxebayarticles', /*
update oxebayarticles e set e.old_stock = (SELECT a.oxstock FROM oxarticles a WHERE a.oxid = "bf41f3f4d7e6c8d475aa765dbbf1eca1") where e.oxid = "bf41f3f4d7e6c8d475aa765dbbf1eca1"*/
    );


    protected function _sqllogsave($query)
    {

        require_once $_SERVER["DOCUMENT_ROOT"]."modules/exonnutils/".DIRECTORY_SEPARATOR."exonnutils.php";

        $query = trim($query);

        $queryType = trim(strtoupper(substr($query,0,7)));

        if (!in_array($queryType,array("UPDATE","DELETE")))
            return;

        $pos_where = $this->_searchStrInNoQuote($query,"where");

        if ($queryType=="UPDATE") {
            $str_pos = $this->_searchStrInNoQuote($query,"set");
            $str_pos1=7;
        }
        else
        {
            $str_pos=((!$pos_where) ? strlen($query) : $pos_where);
            $str_pos1=$this->_searchStrInNoQuote($query,"from");
            $str_pos1+=5;
        }


        $backtrace = exonnutils::debug_backtrace();


        if ($str_pos) {

            if (!$pos_where)
                $where="";
            else
                $where=substr($query,$pos_where);


            $tables=explode(",",substr($query,$str_pos1,($str_pos-$str_pos1)));
            foreach ($tables as $table)
            {
                $table_name_array=explode(" ",trim($table));

                $insert_Tablename = "";
                $table_alias = "";
                foreach($table_name_array as $temp) {
                    if (!$temp || strtolower($temp)=="as")
                        continue;

                    $temp = addslashes(str_replace("`", "", trim($temp)));
                    if (!$insert_Tablename)
                        $insert_Tablename = $temp;
                    else {
                        $table_alias = $temp;
                        break;
                    }
                }

                if (in_array($insert_Tablename, $this->_aTableNoLog))
                    continue;


                // проверка изменяются ли данные в этой таблице
                if ($queryType=="UPDATE") {
                    if ($pos_where)
                        $sSETData = substr($query, $str_pos+3, $pos_where-($str_pos+3));
                    else
                        $sSETData = substr($query, $str_pos+3);


                    $flag_SET_IS = false;
                    $sSETData.=",";
                    while(true) {
                        $pos_coma = $this->_searchStrInNoQuote($sSETData,",", false);
                        if (!$pos_coma) {
                            break;
                        }
                        $aFieldSET = explode("=", substr($sSETData,0,$pos_coma));

                        $sSETData = substr($sSETData,$pos_coma+1);

                        $aFieldNameSET = explode(".", $aFieldSET[0]);

                        if (count($aFieldNameSET)==2) {
                            if ($table_alias<>"") {
                                if (str_replace("`", "", trim($aFieldNameSET[0]))==$table_alias) {
                                    $flag_SET_IS = true;
                                    break;
                                }
                            } else {
                                if (str_replace("`", "", trim($aFieldNameSET[0]))==$insert_Tablename) {
                                    $flag_SET_IS = true;
                                    break;
                                }
                            }
                        } else {
                            $flag_SET_IS = true;
                            break;
                        }

                    }

                    if (!$flag_SET_IS)
                        continue;

                }


                $result=mysql_query( "SHOW COLUMNS FROM ".$insert_Tablename, $this->connectionId);
                $fields = 'arc_ip,arc_user_id,arc_date,arc_url,arc_sql,mybacktrace,class_function';
                while($row = @mysql_fetch_row($result)) {
                    $fields.=",".$row[0];
                }




                $sql_ende=(($table_alias<>"") ? $table_alias."." : $insert_Tablename.".")."*
                                from ".implode(",",$tables).(($table_alias=="" && count($tables)==1) ? " ".$insert_Tablename." " : " ").$where;
                $query_log="insert into ".$insert_Tablename."_arc (".$fields.")
                                select '".addslashes($_SERVER['REMOTE_ADDR'])."' as arc_ip,
                                        '".addslashes($_SESSION["auth"])."' as arc_user_id,
                                        now(),
                                        '".addslashes($_SERVER['REQUEST_URI'])."' as arc_url,
                                        '".addslashes($query)."' as arc_sql,
                                        '"./*addslashes(print_r($backtrace,true)).*/"' as mybacktrace,
                                        '".addslashes($_REQUEST["cl"].":".$_REQUEST["fnc"])."' as class_function,
                                        ".$sql_ende;

                mysql_query($query_log, $this->connectionId);

                $errorno = mysql_errno($this->connectionId);

                switch ($errorno) {
                    case 1146:
                        // если таблицы еще нет ее нужно создать
                        $t = @mysql_fetch_row(mysql_query("SHOW CREATE TABLE ".$insert_Tablename, $this->connectionId));
                        if ($t) {
                            list($temp, $query_create) = $t;

                            $pos_table_create_start = stripos($query_create,'PRIMARY KEY (');
                            if ($pos_table_create_start ) {
                                $query_create = substr($query_create,0,$pos_table_create_start).'PRIMARY KEY (`arc_id`)) ENGINE=MyISAM';
                            }

                            //$query_create = str_ireplace("PRIMARY KEY (`OXID`)", "PRIMARY KEY (`arc_id`)", $query_create);
                            $query_create = str_replace("CREATE TABLE `".$insert_Tablename."` (", "CREATE TABLE `".$insert_Tablename."_arc` (
                                  `arc_id` int NOT NULL AUTO_INCREMENT,
                                  `arc_ip` varchar(20) NOT NULL DEFAULT '',
                                  `arc_user_id` varchar(32) NOT NULL DEFAULT '',
                                  `arc_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
                                  `arc_url` text CHARACTER SET utf8 NOT NULL,
                                  `arc_sql` text CHARACTER SET utf8 NOT NULL,
                                  `mybacktrace` text CHARACTER SET utf8 NOT NULL,
                                  `class_function` varchar(80) NOT NULL DEFAULT '', ",
                                $query_create);

                            mysql_query($query_create, $this->connectionId);
                        }
                        break;

                    case 1054:
                        // нет поля
break;
                        $result_original=mysql_query( "SELECT COLUMN_NAME, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, COLLATION_NAME,   FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='".$insert_Tablename."' order by ORDINAL_POSITION ", $this->connectionId);
                        $aFieldsOriginal = array();
                        while($row = mysql_fetch_assoc($result_original)) {
                            $aFieldsOriginal[]=$row;
                        }


                        $result_arc=mysql_query( "SELECT COLUMN_NAME, COLUMN_DEFAULT, IS_NULLABLE, DATA_TYPE, CHARACTER_MAXIMUM_LENGTH, CHARACTER_SET_NAME, COLLATION_NAME  FROM INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='".$insert_Tablename."_arc' order by ORDINAL_POSITION ", $this->connectionId);
                        $i_arc=0;
                        $i_original=0;
                        while($row_arc = mysql_fetch_assoc($result_arc)) {
                            $i_arc++;
                            if ($i_arc<=6)
                                continue;

                            $aktOrigField = $aFieldsOriginal[$i_original];
                            $i_original++;

                            if ($aktOrigField['COLUMN_NAME'] == $row_arc['COLUMN_NAME'])
                                continue;


                            if ($aktOrigField) {

                                $this->_addFieldToArc($insert_Tablename, $aktOrigField);

                            } else {

                                mysql_query("ALTER TABLE ".$insert_Tablename."_arc DROP `".$row_arc['COLUMN_NAME']."`");

                            }

                        }

                        for($i=$i_original; $i<count($aFieldsOriginal); $i++) {
                            $this->_addFieldToArc($insert_Tablename, $aFieldsOriginal[$i]);
                        }

                        break;

                }

                mysql_query($query_log, $this->connectionId);

                if ($error = mysql_error($this->connectionId)) {
                    $h = fopen($_SERVER["DOCUMENT_ROOT"]."log/errorsqllog.txt", "a");
                    fputs($h,$errorno."\n\n".$query_create."\n\n\n".$error."\n\n\n".$query."\n\n\n".$query_log."\n\n-----------------------------------------\n\n\n\n\n\n" );
                    fclose($h);
                }


            }

        }
    }



    protected function _addFieldToArc($insert_Tablename, $aktOrigField)
    {
        $query_addfield = "ALTER TABLE  ".$insert_Tablename."_arc ADD  `".$aktOrigField['COLUMN_NAME']."` ".$aktOrigField['DATA_TYPE'];
        if ($aktOrigField['CHARACTER_MAXIMUM_LENGTH'])
            $query_addfield.=" ( ".$aktOrigField['CHARACTER_MAXIMUM_LENGTH']." ) ";

        if ($aktOrigField['CHARACTER_SET_NAME'])
            $query_addfield.=" CHARACTER SET ".$aktOrigField['CHARACTER_SET_NAME']." ";

        if ($aktOrigField['COLLATION_NAME'])
            $query_addfield.=" COLLATE ".$aktOrigField['COLLATION_NAME']." ";

        if ($aktOrigField['IS_NULLABLE']=="NO")
            $query_addfield.=" NOT NULL ";

        $query_addfield.=" DEFAULT ".(($aktOrigField['COLUMN_DEFAULT']===null) ? "NULL" : "'".$aktOrigField['COLUMN_DEFAULT']."'");

        mysql_query($query_addfield, $this->connectionId);

    }





    protected function _searchStrInNoQuote($str, $needle, $blWorld=true )
    {

        // убираем из строки двойной слеш
        while(true){
            $str_neu = str_replace('\\\\','\\-', $str);
            if ($str_neu==$str) {
                $str = $str_neu;
                break;
            }
            $str = $str_neu;
        }

        $resfunktion = false;
        $pos_needle = 0;
        $pos_quote=0;

        if ($blWorld)
            $sReg="/\b".$needle."\b/i";
        else
            $sReg="/".$needle."/i";

        while (preg_match($sReg, $str, $res_needle, PREG_OFFSET_CAPTURE, $pos_needle )) {

            $pos_needle = $res_needle[0][1];


            // ищем первую каывчку
            while (preg_match("/(^|[^\\\\])([\"'])/i", $str, $res_quote, PREG_OFFSET_CAPTURE, $pos_quote)) {

                if ($res_quote[2][1]>$pos_needle) {
                    // $res_needle - правильный
                    $resfunktion = $res_needle;
                    break 2;
                }

                // ищем вторую кавычку (substr - необходимо так как если в кавычках пустая строка, то работать не будет
                $temp_pos = $res_quote[2][1]+1;
                preg_match("/(^|[^\\\\])(".$res_quote[2][0].")/i", substr($str,$temp_pos), $res_quote_2, PREG_OFFSET_CAPTURE);
                if (!$res_quote_2[2])  {
                    // вторая кавычка не найдена. Ошибка в строке.
                    return false;
                }

                $pos_quote_2 = $temp_pos + $res_quote_2[2][1];

                if ($pos_quote_2>$pos_needle) {
                    // $res_needle - находится внутри кавычек
                    $pos_needle = $pos_quote_2;
                    continue 2;
                }

                $pos_quote = $pos_quote_2+1;

            }

            // $res_needle - правильный
            $resfunktion = $res_needle;
            break;

        }

        return $resfunktion[0][1];
    }

}