<?php
/**
 *    This file is part of OXID eShop Community Edition.
 *
 *    OXID eShop Community Edition is free software: you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation, either version 3 of the License, or
 *    (at your option) any later version.
 *
 *    OXID eShop Community Edition is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with OXID eShop Community Edition.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @package   admin
 * @copyright (C) OXID eSales AG 2003-2013
 * @version OXID eShop CE
 */

/**
 * Admin user list manager.
 * Performs collection and managing (such as filtering or deleting) function.
 * Admin Menu: User Administration -> Users.
 * @package admin
 */
class exonnconnector_oxuser extends exonnconnector_oxuser_parent
{


    public function createUser()
    {

        // wenn kunde bereits als Gast regestriert wurde, ersetzen wir sein email durch eindeutige zeichenkette,
        // damit diese zeile durch standarte OXID function nicht gelöscht wird!
        $oDb = oxDb::getDb();
        $sShopID = $this->getConfig()->getShopId();

        $sSelect = "select oxid from oxuser where oxusername = " . $oDb->quote( $this->oxuser__oxusername->value ) . " and oxpassword = '' ";
        if ( !$this->_blMallUsers ) {
            $sSelect .= " and oxshopid = '{$sShopID}' ";
        }
        $sOXID = $oDb->getOne( $sSelect, false, false );
        if ( isset( $sOXID ) && $sOXID ) {
            $newUserName = $this->oxuser__oxusername->value."_".str_replace(" ",".",microtime());
            $oDb->execute("update oxuser set oxusername=".$oDb->quote($newUserName)." where oxid=".$oDb->quote($sOXID));
        }
        //********************** end **********************************************/*/*/*/

        $res = parent::createUser();

        if (!$this->oxuser__oxcustnr->value)
            $this->_setNumber();

        return $res;
    }


    public function save()
    {
        $res = parent::save();

        if (!$this->oxuser__oxcustnr->value)
            $this->_setNumber();


        return $res;
    }



    protected function _setNumber()
    {

        $oDB = oxDb::getDb();

        $iUserNummerLength = oxDb::getDb()->getOne("select oxvalue from exonnwawi_config where oxvar='sExUserNummerLength' ");
        $iPrefix = oxDb::getDb()->getOne("select oxvalue from exonnwawi_config where oxvar='sExPrefixNummer' ");

        $oDB->Execute("LOCK TABLES oxuser WRITE");


        if (!$iUserNummerLength)
            $iUserNummerLength=7;

        if ($this->getConfig()->getConfigParam('sRandNummerCreate')) {

            do {
                $iRegister = pow(10,($iUserNummerLength-2));

                $StartNummer = 1;
                $EndNummer = $iRegister - 1;

                $iNum='1'.$iPrefix.str_pad(rand($StartNummer,$EndNummer), $iUserNummerLength-2, '0', STR_PAD_LEFT);

                $sCheck = "select oxcustnr from oxuser where oxcustnr='$iNum'  ";
                $iC = oxDb::getDb(true)->GetOne( $sCheck );
                if ( $iC > 1 )
                    continue;

                $iChkCnt=$iNum;

                break;

            }while(true);


        } else {

            $iRegister = pow(10,($iUserNummerLength-2));

            $StartNummer = ("1".$iPrefix) * $iRegister;
            $EndNummer = ("1".($iPrefix+1)) * $iRegister - 1;

            $iChkCnt = oxDb::getDb(true)->GetOne( "select max(oxcustnr) from oxuser where oxcustnr>=".$StartNummer." && oxcustnr<".$EndNummer );
            if ( !$iChkCnt  ) {
                $iChkCnt=$StartNummer;
            } else {
                $iChkCnt++;
            }

            $iNumFromlog = @file_get_contents($this->getLogFilePath());
            if ($iChkCnt<=$iNumFromlog) {
                $iChkCnt++;
            }
        }

        $sUpdate = "update oxuser set oxcustnr=".$iChkCnt." where oxid = ".$oDB->quote($this->getId());
        oxDb::getDb()->Execute( $sUpdate );

        $this->oxuser__oxcustnr = new oxField($iChkCnt, oxField::T_RAW);

        file_put_contents($this->getLogFilePath(), $iChkCnt);

        $oDB->Execute("UNLOCK TABLES");

    }

    protected function getLogFilePath()
    {
        $oConfig = $this->getConfig();
        if (!file_exists($oConfig->getOutDir()."exonn_documents/exonn_shopoxidconnector")) {
            if (!file_exists($oConfig->getOutDir()."exonn_documents")) {
                mkdir($oConfig->getOutDir()."exonn_documents");
            }

            mkdir($oConfig->getOutDir()."exonn_documents/exonn_shopoxidconnector");

        }

        return $oConfig->getOutDir()."exonn_documents/exonn_shopoxidconnector/data.txt";
    }



}