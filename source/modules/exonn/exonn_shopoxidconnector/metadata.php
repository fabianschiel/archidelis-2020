<?php

/**
 * Metadata version
 */
$sMetadataVersion = '1.0';

if (file_exists('../modules/exonn/exonn_shopoxidconnector/models/exonnconnector_oxid.php'))
    $sDir = 'exonn/';
else
    $sDir ='';


/**
 * Module information
 */
$aModule = array(
    'id'          => 'exonn_shopoxidconnector',
    'title'       => 'EXONN WaWi OXID-Shop-Connector Modul',
    'description' => 'EXONN WaWi OXID-Shop-Connector Modul',
    'thumbnail'   => 'exonn_logo.png',
    'version'     => '3.2.7',
    'author'      => 'EXONN',
    'email'       => 'info@exonn.de',
    'url'         => 'http://www.oxidmodule24.de/',
    'events'       => array(
        'onActivate' 	=> 'exonnconnector_event::onActivate',
        'onDeactivate' 	=> 'exonnconnector_event::onDeactivate',
    ),
    'extend'      => array(
        'oxuser' => $sDir.'exonn_shopoxidconnector/models/exonnconnector_oxuser',
        'oxorder' => $sDir.'exonn_shopoxidconnector/models/exonnconnector_oxorder',
        'oxdeliverylist' => $sDir.'exonn_shopoxidconnector/models/exonn_delext_oxdeliverylist',
        'oxdelivery'     => $sDir.'exonn_shopoxidconnector/models/exonn_delext_oxdelivery',
    ),
    'files' => array(
        'exonnconnector_oxid' => $sDir.'exonn_shopoxidconnector/models/exonnconnector_oxid.php',
        'exonnconnector_event' => $sDir.'exonn_shopoxidconnector/models/exonnconnector_event.php',
        'oxbankingkonten' => $sDir.'exonn_shopoxidconnector/models/oxbankingkonten.php',
    ),
    'templates' => array(
    ),

);

