<tr>
	<td class="edittext">[{oxmultilang ident="ORDER_OVERVIEW_PAYMENTTYPE"}]: </td>
	<td class="edittext"><b>[{$paymentType->oxpayments__oxdesc->value}]</b></td>
</tr>
<tr>
	<td class="edittext">[{oxmultilang ident="ORDER_OVERVIEW_DELTYPE"}]: </td>
	<td class="edittext"><b>[{$deliveryType->oxdeliveryset__oxtitle->value}]</b><br></td>
</tr>

[{ if $paymentType->oxpayments__oxdesc->value=="Kreditkarte"}]
 <tr>
	<td class="edittext">Kreditkartendaten l&ouml;schen [{$paymentType->oxpayments__oxid->value}]  </td>
	<td class="edittext">
	<form name="phcreditcard" id="myedit" action="[{ $oViewConf->getSelfLink() }]" method="post">
	[{ $oViewConf->getHiddenSid() }]
	<input type="hidden" name="cl" value="order_overview">
	<input type="hidden" name="fnc" value="deletecreditcardinfo">
	<input type="hidden" name="oxid" value="[{ $oxid }]">
	<input type="submit" value="l&ouml;schen" onclick="javascript:if (confirm('Kreditkartendaten wirklich entfernen')==true) {return true;} else {return false;}">
		</form>
	</td>
</tr>
[{/if}]