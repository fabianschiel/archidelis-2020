<?php

/**
 * @TODO LICENCE HERE
 */

/**
 * Metadata version
 */
$sMetadataVersion = '2.0';

/**
 * Module information
 */
$aModule = array(
	'id'          => 'd1delccModule',
	'title'       => array(
		'de' => 'Kreditkartendaten Löschen Modul',
		'en' => 'Delete Credit Card Data Module',
	),
	'description' => array(
		'de' => '<h2>Kreditkartendaten Löschen Modul von Dialog One</h2>',
		'en' => '<h2>Delete Credit Card Data Module</h2>',
	),
	'thumbnail'   => '',
	'version'     => '1.0.0',
	'author'      => 'Fabian Schiel | Dialog-One',
	'url'         => 'https://www.dialog-one.at',
	'email'       => 'fschiel@dialog-one.at',
	'extend'      => array(
		\OxidEsales\Eshop\Application\Controller\Admin\OrderOverview::class =>
			'd1/delcc/ext/D1delccOrderOverview'
	),
	'files'       => array('D1DelCCOrderOverview' => 'd1/delcc/ext/D1delccOrderOverview.php'),
	'templates'   => array(
		'main.tpl' => 'd1/d1delccModule/views/admin/main.tpl'
	),
	'blocks'      => array(
		[
			'template' => 'order_overview.tpl',
			'block' => 'admin_order_overview_checkout',
			'file' => 'application/views/admin/blocks/admin_order_overview_checkout.tpl'
		]
	),
	'settings'    => array(),
	'events'      => array(),
);