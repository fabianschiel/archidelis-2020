<?php

/**
 * Extension for OXID order_overview controller
 *
 * @author Fabian Schiel, Dialog-One Marketing GmbH
 */

class D1delccOrderOverview extends D1delccOrderOverview_parent
{

    /**
     * Delete Credit Card Info from oxuserpayments
     * @throws Exception
     */
    public function deletecreditcardinfo() {

        $soxId = $this->getEditObjectId();
        $oOrder = oxNew( "oxorder" );
        if ( $oOrder->load( $this->getEditObjectId() ) ) {
            $sPaymentID=$oOrder->oxorder__oxpaymentid->value;
            $sSql="Update oxuserpayments set oxvalue=NULL where oxid='$sPaymentID'";
            \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->Execute($sSql);
        }


    }

}