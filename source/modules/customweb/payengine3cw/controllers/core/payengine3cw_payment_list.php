<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_PayEngine3Cw
 * @version		1.0.200
 */



class payengine3cw_payment_list extends payengine3cw_payment_list_parent
{
	public function render()
	{
		parent::render();

		$this->_aViewData['enabledModule'] = false;
		$this->_aViewData['noSelection'] = false;

		$paymentId = $this->getEditObjectId();
		if ($paymentId != '-1' && isset($paymentId)) {
            if (PayEngine3CwHelper::isPayengine3cwPaymentMethod($paymentId)) {
            	$this->_aViewData['enabledModule'] = 'payengine3cw';
            }
		} else {
			$this->_aViewData['noSelection'] = true;
		}

		return "payengine3cw_payment_list.tpl";
	}
}