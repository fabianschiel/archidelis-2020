<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_PayEngine3Cw
 * @version		1.0.200
 */

PayEngine3CwHelper::bootstrap();

require_once 'Customweb/Cron/Processor.php';


class payengine3cw_cron extends oxUBase
{
	public function render()
	{
		PayEngine3CwHelper::cleanupTransactions();

		try {
			$packages = array(
			0 => 'Customweb_PayEngine3',
 			1 => 'Customweb_Payment_Authorization',
 		);
			$packages[] = 'PayEngine3Cw';
			$packages[] = 'Customweb_Payment_Update_ScheduledProcessor';
			$cronProcessor = new Customweb_Cron_Processor(PayEngine3CwHelper::createContainer(), $packages);
			$cronProcessor->run();
		} catch (Exception $e) {}

		die();
	}
}