[{if $payengine3cw_widgets}]
	<div class="lineBox">
		[{foreach from=$payengine3cw_widgets item=widget}]
			<div class="payengine3cw-external-checkout-widget">
				[{$widget.html}]
			</div>
		[{/foreach}]
	</div>

	<style type="text/css">
	.payengine3cw-external-checkout-widget {
		display: inline-block;
  		margin-right: 15px;
  		margin-bottom: 15px;
	}
	</style>
[{/if}]

[{$smarty.block.parent}]