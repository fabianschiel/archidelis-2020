[{oxstyle include=$oViewConf->getModuleUrl('payengine3cw', 'out/src/css/payment_form.css')}]

[{if $paymentmethod->isPayengine3cwPaymentMethod()}]
	<div id="paymentOption_[{$sPaymentID}]" class="payment-option [{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]active-payment[{/if}]">
		<input id="payment_[{$sPaymentID}]" type="radio" name="paymentid" value="[{$sPaymentID}]" [{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]checked="checked"[{/if}] />
	    <ul class="form">
	        [{if $paymentmethod->getPrice()}]
	            <li>
	                <div class="payment-charge">
	                    [{if $oxcmp_basket->getPayCostNet()}]
	                        ([{$paymentmethod->getFNettoPrice()}] [{$currency->sign}] [{oxmultilang ident="PLUS_VAT"}] [{$paymentmethod->getFPriceVat()}] )
	                    [{else}]
	                        ([{$paymentmethod->getFBruttoPrice()}] [{$currency->sign}])
	                    [{/if}]
	                </div>
	            </li>
	        [{/if}]

			[{block name="checkout_payment_longdesc"}]
	            [{if $paymentmethod->oxpayments__oxlongdesc->value}]
	                <li>
	                    <div class="payment-desc">
	                        [{$paymentmethod->oxpayments__oxlongdesc->getRawValue()}]
	                    </div>
	                </li>
	            [{/if}]
	        [{/block}]
	        
	        [{if $paymentmethod->isPayengine3cwPaymentFormOnPaymentPage()}]
	        	<li>
		        	<div class="payengine3cw-payment-form [{$sPaymentID}]-form" data-authorization-method="[{$paymentmethod->getPayengine3cwAuthorizationMethod()}]">
						<ul class="form">
							<div class="payengine3cw-alias-form-fields">
								[{$paymentmethod->getPayengine3cwAliasFormFields()}]
							</div>
							
							<div class="payengine3cw-visible-form-fields">
								[{$paymentmethod->getPayengine3cwVisibleFormFields()}]
							</div>
						</ul>
					</div>
				</li>
			[{/if}]
	    </ul>
	</div>
[{else}]
    [{$smarty.block.parent}]
[{/if}]