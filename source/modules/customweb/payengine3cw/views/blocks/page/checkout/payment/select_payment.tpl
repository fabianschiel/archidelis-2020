[{oxstyle include=$oViewConf->getModuleUrl('payengine3cw', 'out/src/css/payment_form.css')}]

[{if $paymentmethod->isPayengine3cwPaymentMethod()}]
    <dl>
        <dt>
            <input id="payment_[{$sPaymentID}]" type="radio" name="paymentid" value="[{$sPaymentID}]" [{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]checked[{/if}]>
            <label for="payment_[{$sPaymentID}]"><b>[{ $paymentmethod->oxpayments__oxdesc->value}] [{ if $paymentmethod->fAddPaymentSum }]([{ $paymentmethod->fAddPaymentSum }] [{ $currency->sign}])[{/if}]</b></label>
        </dt>
        <dd class="[{if $oView->getCheckedPaymentId() == $paymentmethod->oxpayments__oxid->value}]activePayment[{/if}]">
        	[{if $paymentmethod->oxpayments__oxlongdesc->value}]
                <div class="desc">
                    [{ $paymentmethod->oxpayments__oxlongdesc->getRawValue()}]
                </div>
            [{/if}]
        	[{if $paymentmethod->isPayengine3cwPaymentFormOnPaymentPage()}]
	        	<div class="payengine3cw-payment-form [{$sPaymentID}]-form" data-authorization-method="[{$paymentmethod->getPayengine3cwAuthorizationMethod()}]">
					<ul class="form">
						<div class="payengine3cw-alias-form-fields">
							[{$paymentmethod->getPayengine3cwAliasFormFields()}]
						</div>
						
						<div class="payengine3cw-visible-form-fields">
							[{$paymentmethod->getPayengine3cwVisibleFormFields()}]
						</div>
					</ul>
				</div>
			[{/if}]
        </dd>
    </dl>
[{else}]
    [{$smarty.block.parent}]
[{/if}]