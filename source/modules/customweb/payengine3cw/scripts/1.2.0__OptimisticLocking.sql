ALTER TABLE `payengine3cw_transaction` ADD `versionNumber` int NOT NULL;
ALTER TABLE `payengine3cw_transaction` ADD `liveTransaction` char(1);
ALTER TABLE `payengine3cw_customer_context` ADD `versionNumber` int NOT NULL;
ALTER TABLE `payengine3cw_external_checkout_context` ADD `versionNumber` int NOT NULL;