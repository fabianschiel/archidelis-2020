<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_PayEngine3Cw
 * @version		1.0.200
 */

PayEngine3CwHelper::bootstrap();

require_once 'Customweb/Database/Migration/Manager.php';


class PayEngine3CwSetup
{
	/**
	 * Execute action on activate event
	 */
	public static function onActivate()
	{
		$scriptDir = dirname(dirname(__FILE__)) . '/scripts/';
		$manager = new Customweb_Database_Migration_Manager(PayEngine3CwHelper::getDriver(), $scriptDir, 'payengine3cw_schema_version');
		$manager->migrate();

		self::installPaymentMethods();
		PayEngine3CwHelper::cleanupTransactions();
	}

	/**
	 * Execute action on deactivate event
	 */
	public static function onDeactivate()
	{

	}

	private static function installPaymentMethods()
	{
		$paymentMethods = array(
			'payengine3cw_wechatpay' => array(
				'name' => 'payengine3cw_wechatpay',
 				'description' => 'WeChat Pay',
 			),
 			'payengine3cw_alipay' => array(
				'name' => 'payengine3cw_alipay',
 				'description' => 'Alipay',
 			),
 			'payengine3cw_paypal' => array(
				'name' => 'payengine3cw_paypal',
 				'description' => 'PayPal',
 			),
 			'payengine3cw_postfinancecard' => array(
				'name' => 'payengine3cw_postfinancecard',
 				'description' => 'PostFinance Card',
 			),
 			'payengine3cw_paydirekt' => array(
				'name' => 'payengine3cw_paydirekt',
 				'description' => 'paydirekt',
 			),
 			'payengine3cw_ratepayopeninvoice' => array(
				'name' => 'payengine3cw_ratepayopeninvoice',
 				'description' => 'RatePay Open Invoice',
 			),
 			'payengine3cw_giropay' => array(
				'name' => 'payengine3cw_giropay',
 				'description' => 'giropay',
 			),
 			'payengine3cw_sofortueberweisung' => array(
				'name' => 'payengine3cw_sofortueberweisung',
 				'description' => 'SOFORT',
 			),
 			'payengine3cw_creditcard' => array(
				'name' => 'payengine3cw_creditcard',
 				'description' => 'Credit / Debit Card',
 			),
 			'payengine3cw_ratepayinstallments' => array(
				'name' => 'payengine3cw_ratepayinstallments',
 				'description' => 'RatePay Installments',
 			),
 			'payengine3cw_bcmc' => array(
				'name' => 'payengine3cw_bcmc',
 				'description' => 'Bancontact',
 			),
 			'payengine3cw_eps' => array(
				'name' => 'payengine3cw_eps',
 				'description' => 'EPS',
 			),
 			'payengine3cw_ratepaydirectdebits' => array(
				'name' => 'payengine3cw_ratepaydirectdebits',
 				'description' => 'RatePay Direct Debits',
 			),
 			'payengine3cw_ideal' => array(
				'name' => 'payengine3cw_ideal',
 				'description' => 'iDEAL',
 			),
 		);

		$driver = PayEngine3CwHelper::getDriver();
		foreach ($paymentMethods as $paymentMethod) {
			$oPayment = oxNew('oxPayment');
			if(!$oPayment->load($paymentMethod['name'])) {
				$driver->query("INSERT INTO `oxpayments` (
					`OXID`, `OXACTIVE`, `OXDESC`, `OXDESC_1`, `OXTOAMOUNT`
				) VALUES (
					>name, 0, >description, >description, 1000000
				)")->execute(array(
						'>name' => $paymentMethod['name'],
						'>description' => $paymentMethod['description']
					));
			}
		}
	}
}