<?php
/**
 * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 *
 * @category	Customweb
 * @package		Customweb_PayEngine3Cw
 * @version		1.0.200
 */

PayEngine3CwHelper::bootstrap();

require_once 'Customweb/Util/Encoding.php';


/**
 * @Bean
 */
class PayEngine3CwAdapterIframeAdapter extends PayEngine3CwAdapterAbstractAdapter
{
	private $formActionUrl = null;

	public function getPaymentAdapterInterfaceName() {
		return 'Customweb_Payment_Authorization_Iframe_IAdapter';
	}

	/**
	 * @return Customweb_Payment_Authorization_Iframe_IAdapter
	 */
	public function getInterfaceAdapter() {
		return parent::getInterfaceAdapter();
	}

	protected function prepareAdapter() {
		$this->formActionUrl = PayEngine3CwHelper::getUrl(array(
			'cl' => 'payengine3cw_iframe',
			'cstrxid' => $this->getTransaction()->getTransactionId()
		));
		PayEngine3CwHelper::getEntityManager()->persist($this->getTransaction());
	}

	public function processOrderConfirmationRequest() {
		$vars = array(
			'formActionUrl' => $this->formActionUrl
		);
		return $vars;
	}

	public function getIframeTemplateVars() {
		$vars = array(
			'targetUrl' => $this->getInterfaceAdapter()->getIframeUrl($this->getTransaction()->getTransactionObject(), Customweb_Util_Encoding::toUTF8($_REQUEST)),
			'iframeHeight' => $this->getInterfaceAdapter()->getIframeHeight($this->getTransaction()->getTransactionObject(), Customweb_Util_Encoding::toUTF8($_REQUEST))
		);
		PayEngine3CwHelper::getEntityManager()->persist($this->getTransaction());
		return $vars;
	}

	protected function getFormActionUrl() {
		return $this->formActionUrl;
	}
}