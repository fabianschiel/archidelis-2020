[{include file="headitem.tpl" title="GENERAL_ADMIN_TITLE"|oxmultilangassign box="list"}]
[{assign var="where" value=$oView->getListFilter()}]

[{if $readonly}]
    [{assign var="readonly" value="readonly disabled"}]
[{else}]
    [{assign var="readonly" value=""}]
[{/if}]

<script type="text/javascript">
<!--
window.onload = function ()
{
    top.reloadEditFrame();
    [{if $updatelist == 1}]
        top.oxid.admin.updateList('[{$oxid}]');
    [{/if}]
}
//-->
</script>


<div id="liste">

<form name="search" id="search" action="[{$oViewConf->getSelfLink()}]" method="post">
[{include file="_formparams.tpl" cl="cookiemanager_list" lstrt=$lstrt actedit=$actedit oxid=$oxid fnc="" language=$actlang editlanguage=$actlang}]
<table cellspacing="0" cellpadding="0" border="0" width="100%">
    <colgroup>
        [{block name="admin_cookiemanager_list_colgroup"}]
            <col width="7%">
            <col width="43%">
            <col width="45%">
            <col width="5%">
        [{/block}]
    </colgroup>

<tr class="listitem">
    [{block name="admin_cookiemanager_list_filter"}]
        <td class="listfilter first" height="20">&nbsp;</td>
        <td class="listfilter" height="20">
            <input class="listedit" type="text" size="50" maxlength="128" name="where[netcookiemanager][oxtitle]" value="[{ $where.netcookiemanager.oxtitle }]" placeholder="Nach Cookie Bezeichnung filtern">
        </td>
        <td class="listfilter" height="20">
            <div class="r1">
                <div class="b1">
                    <select class="folderselect" name="where[netcookiemanager][oxgroup]">
                        <option value="">---</option>
                        <option value="0" [{if $where.netcookiemanager.oxgroup == "0"}]selected[{/if}]>[{oxmultilang ident="NET_COOKIE_MANAGER_MAIN_GROUPNAME_0"}]</option>
                        <option value="1" [{if $where.netcookiemanager.oxgroup == "1"}]selected[{/if}]>[{oxmultilang ident="NET_COOKIE_MANAGER_MAIN_GROUPNAME_1"}]</option>
                        <option value="2" [{if $where.netcookiemanager.oxgroup == "2"}]selected[{/if}]>[{oxmultilang ident="NET_COOKIE_MANAGER_MAIN_GROUPNAME_2"}]</option>
                    </select>

                </div>
            </div>
        </td>
        <td class="listfilter" height="20">
            <input class="listedit" type="submit" name="submitit" value="[{oxmultilang ident="GENERAL_SEARCH"}]">
        </td>
    [{/block}]
</tr>
<tr class="listitem">
    [{block name="admin_cookiemanager_list_sorting"}]
        <td class="listheader first" height="15" width="30" align="center"><a href="Javascript:top.oxid.admin.setSorting( document.search, 'netcampaigns', 'oxactive', 'asc');document.search.submit();" class="listheader">[{ oxmultilang ident="GENERAL_ACTIVTITLE" }]</a></td>
        <td class="listheader" height="15"><a href="Javascript:top.oxid.admin.setSorting( document.search, 'netcookiemanager', 'oxtitle', 'asc');document.search.submit();" class="listheader">[{oxmultilang ident="NET_GENERAL_COOKIE_TITLE"}]</a></td>
        <td class="listheader" height="15"><a href="Javascript:top.oxid.admin.setSorting( document.search, 'netcookiemanager', 'oxgroup', 'asc');document.search.submit();" class="listheader">[{oxmultilang ident="NET_GENERAL_COOKIE_GROUP"}]</a></td>
        <td class="listheader"></td>
    [{/block}]
</tr>

[{assign var="blWhite" value=""}]
[{assign var="_cnt" value=0}]
[{foreach from=$mylist item=listitem}]
    [{assign var="_cnt" value=$_cnt+1}]
    <tr id="row.[{$_cnt}]">

    [{if $listitem->blacklist == 1}]
        [{assign var="listclass" value=listitem3 }]
    [{else}]
        [{assign var="listclass" value=listitem$blWhite }]
    [{/if}]
    [{if $listitem->getId() == $oxid }]
        [{assign var="listclass" value=listitem4 }]
    [{/if}]
    [{block name="admin_cookiemanager_list_item"}]
        <td valign="top" class="[{$listclass}][{if $listitem->netcookiemanager__oxactive->value == 1}] active[{/if}]" height="15"><div class="listitemfloating">&nbsp;</a></div></td>
        <td valign="top" class="[{$listclass}]" height="15"><div class="listitemfloating"><a href="Javascript:top.oxid.admin.editThis('[{$listitem->netcookiemanager__oxid->value}]');" class="[{$listclass}]">[{$listitem->netcookiemanager__oxtitle->value}]</a></div></td>
        <td valign="top" class="[{$listclass}]" height="15">
            <div class="listitemfloating">
                <a href="Javascript:top.oxid.admin.editThis('[{$listitem->netcookiemanager__oxid->value}]');" class="[{$listclass}]">
                    [{if $listitem->netcookiemanager__oxgroup->value == 0}]
                        [{oxmultilang ident="NET_COOKIE_MANAGER_MAIN_GROUPNAME_0"}]
                    [{elseif $listitem->netcookiemanager__oxgroup->value == 1}]
                        [{oxmultilang ident="NET_COOKIE_MANAGER_MAIN_GROUPNAME_1"}]
                    [{elseif $listitem->netcookiemanager__oxgroup->value == 2}]
                        [{oxmultilang ident="NET_COOKIE_MANAGER_MAIN_GROUPNAME_2"}]
                    [{/if}]
                </a>
            </div>
        </td>
        <td class="[{$listclass}]"><a href="Javascript:top.oxid.admin.deleteThis('[{$listitem->netcookiemanager__oxid->value}]');" class="delete" id="del.[{$_cnt}]" [{include file="help.tpl" helpid=item_delete}]></a></td>
    [{/block}]
</tr>
[{if $blWhite == "2"}]
[{assign var="blWhite" value=""}]
[{else}]
[{assign var="blWhite" value="2"}]
[{/if}]
[{/foreach}]
[{include file="pagenavisnippet.tpl" colspan="4"}]
</table>
</form>
</div>

[{include file="pagetabsnippet.tpl"}]


<script type="text/javascript">
if (parent.parent)
{   parent.parent.sShopTitle   = "[{$actshopobj->oxshops__oxname->getRawValue()|oxaddslashes}]";
    parent.parent.sMenuItem    = "[{ oxmultilang ident="NET_COOKIEMANAGER_LIST_MENUITEM" }]";
    parent.parent.sMenuSubItem = "[{ oxmultilang ident="NET_COOKIEMANAGER_LIST_MENUSUBITEM" }]";
    parent.parent.sWorkArea    = "[{$_act}]";
    parent.parent.setTitle();
}
</script>
</body>
</html>

