<?php

$sLangName = 'English';

$aLang = array(
    'charset'                                       => 'utf-8',
    'NETENSIO_NAVIGATION'                           => 'Netensio Software',
    'netcookiemanager'                              => 'Cookie Manager',
    'cookiemanager'                                 => 'Cookie Manager',
    'cookiemanager_list'                            => 'Cookie Manager - Cookie list',
    'cookiemanager_main'                            => 'Manage cookies',

    'NET_COOKIEMANAGER_LIST_MENUITEM'               => 'Cookie Manager',
    'NET_COOKIEMANAGER_LIST_MENUSUBITEM'            => 'Cookie acceptance admin',

    'NET_COOKIE_MANAGER_MAIN_ACTIVE'                => 'Active',
    'NET_COOKIE_MANAGER_MAIN_COOKIEID'              => 'Cookie ID',
    'NET_COOKIE_MANAGER_MAIN_TITLE'                 => 'Cookie name',
    'NET_COOKIE_MANAGER_MAIN_DESC'                  => 'Description',
    'NET_COOKIE_MANAGER_MAIN_GROUP'                 => 'Cookie category',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_0'           => 'Functional',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_1'           => 'Marketing',
    'NET_COOKIE_MANAGER_MAIN_GROUPNAME_2'           => 'Essential',
    'NET_COOKIE_MANAGER_MAIN_SORT'                  => 'Sorting',
    'NET_COOKIE_MANAGER_MAIN_SAVE'                  => 'Save',

    'NET_COOKIEMANAGER_NEWCOOKIE'                   => 'New cookie entry',

    'NET_GENERAL_COOKIE_TITLE'                      => 'Cookie title',
    'NET_GENERAL_COOKIE_GROUP'                      => 'Cookie category',

);
