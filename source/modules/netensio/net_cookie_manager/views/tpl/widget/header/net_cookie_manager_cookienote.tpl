[{if $oView->isEnabled() && $smarty.cookies.displayedCookiesNotification != '1'}]
    [{oxscript include="js/libs/jquery.cookie.min.js"}]
    [{capture assign="sJsCookieConsentOpener"}]
        [{strip}]
        $(document).ready(function() {
            if ($.cookie('cookienote') !== "set") {
                $('#cookieNote').modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $.cookie('cookienote', 'unset', { path: '/' });
                $('#cookieNote').modal('show');
            }
            $("#adjust-cookie-settings").click(function () {
                $("#adjust-cookie-settings").css("display", "none");
                $("#hide-cookie-settings").css("display", "inline");
                $(".cookienote-welcometext").css("display", "none");
                $("#cookie-accordion").css("display", "block");
                return false;
            });

            $("#hide-cookie-settings").click(function () {
                $("#hide-cookie-settings").css("display", "none");
                $("#adjust-cookie-settings").css("display", "inline");
                $("#cookie-accordion").css("display", "none");
                $(".cookienote-welcometext").css("display", "block");
                return false;
            });

            $("#saveCookieSelection").click(function () {
                $.ajax({
                    type: "POST",
                    url: $("#cookieSelectionForm").attr('action'),
                    data: $("#cookieSelectionForm").serialize(),
                    success: function(data) {
                        $.cookie('cookienote', 'set', { path: '/' });
                        $('#cookieNote').modal('hide');
                        location.reload();
                    }
                });
                return false;
            });
        });
        [{/strip}]
    [{/capture}]

    [{oxscript add=$sJsCookieConsentOpener priority=10}]

    <div id="cookieNote" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h4 class="modal-title">[{oxmultilang ident="NET_COOKIE_MANAGER_POPUP_HEADLINE"}]</h4>
                </div>
                <form id="cookieSelectionForm" action="[{$oViewConf->getSelfActionLink()}]" method="get">
                    [{$oViewConf->getNavFormParams()}]
                    [{$oViewConf->getHiddenSid()}]
                    <input type="hidden" name="cl" value="net_cookie_manager_frontend_controller">
                    <input type="hidden" name="fnc" value="saveuserselection">
                    [{assign var="aCookieGroups" value=$oView->getCookieGroups()}]
                    <div class="modal-body">
                        <p class="cookienote-welcometext">[{oxmultilang ident="NET_COOKIE_NOTE_WELCOMETEXT"}]</p>
                        [{if (is_array($aCookieGroups) || is_object($aCookieGroups)) && $aCookieGroups|@count gt 0}]
                            <div class="panel-group row" id="cookie-accordion" role="tablist" aria-multiselectable="true" style="display:none;">
                                [{foreach from=$aCookieGroups item="sCookieGroup" name="cookiegroups"}]
                                    <div class="panel panel-default">
                                        <div class="panel-heading" role="tab" id="heading[{$smarty.foreach.cookiegroups.iteration}]">
                                            <h4 class="panel-title">
                                                <a role="button" data-toggle="collapse" data-parent="#cookie-accordion" href="#collapse[{$smarty.foreach.cookiegroups.iteration}]" aria-expanded="true" aria-controls="collapse[{$smarty.foreach.cookiegroups.iteration}]">
                                                    [{assign var="sCookieManagerGroupPrexix" value="NET_COOKIE_MANAGER_MAIN_GROUPNAME_"}]
                                                    [{oxmultilang ident=$sCookieManagerGroupPrexix|cat:$sCookieGroup}] <i class="fa fa-caret-down pull-right"></i>
                                                </a>
                                            </h4>
                                        </div>
                                        <div id="collapse[{$smarty.foreach.cookiegroups.iteration}]" class="panel-collapse collapse[{if $smarty.foreach.cookiegroups.iteration == 1}] in[{/if}]" role="tabpanel" aria-labelledby="heading[{$smarty.foreach.cookiegroups.iteration}]">
                                            <ul class="panel-body list list-unstyled">
                                                [{assign var="oGroupCookieList" value=$oView->getGroupCookieList($sCookieGroup)}]
                                                [{foreach from=$oGroupCookieList item="oCookie" name="cookiegroupinner"|cat:$sCookieGroup}]
                                                    <li>
                                                        <label class="btn-block">
                                                            [{if $sCookieGroup != 2}]
                                                                <input type="hidden" name="aCookieSel[[{$oCookie->netcookiemanager__oxcookieid->value}]]" value="0">
                                                            [{else}]
                                                                <input type="hidden" name="aCookieSel[[{$oCookie->netcookiemanager__oxcookieid->value}]]" value="1">
                                                            [{/if}]
                                                            <input type="checkbox" name="aCookieSel[[{$oCookie->netcookiemanager__oxcookieid->value}]]" value="1" checked [{if $sCookieGroup == 2}]disabled="disabled"[{/if}]><strong>[{$oCookie->netcookiemanager__oxtitle->value}]</strong>
                                                        </label>
                                                    </li>
                                                [{/foreach}]
                                            </ul>
                                        </div>
                                    </div>
                                [{/foreach}]
                            </div>
                        [{/if}]
                    </div>
                    <div class="modal-footer">
                        <button id="hide-cookie-settings" type="button" class="btn btn-default btn-md" style="display:none">[{oxmultilang ident='NET_COOKIE_NOTE_HIDE_SETTINGS'}]</button><button id="adjust-cookie-settings" type="button" class="btn btn-default btn-md">[{oxmultilang ident='NET_COOKIE_NOTE_ADJUST_SETTINGS'}]</button> <button id="saveCookieSelection" type="button" class="btn btn-primary btn-md"><strong>[{oxmultilang ident='NET_COOKIE_NOTE_CLOSE'}]</strong></button>
                    </div>
                </form>
            </div>
        </div>
    </div>
[{/if}]
[{oxscript widget=$oView->getClassName()}]