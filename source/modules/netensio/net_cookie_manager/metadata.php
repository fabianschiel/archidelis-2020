<?php

$sMetadataVersion = '2.1';

$aModule = [
    'id' => 'net_cookie_manager',
    'title' => 'Netensio: Cookie Manager',
    'description' => [
        'de' => 'Cookie Manager für OXID eShop',
        'en' => 'Cookie Manager for OXID eShop',
    ],
    'thumbnail' => 'netensio.jpg',
    'version' => '1.0.2',
    'author' => 'Netensio',
    'url' => 'https://www.netensio.de',
    'email' => 'info@netensio.de',
    'extend' => [
        \OxidEsales\Eshop\Core\Language::class =>
            \Netensio\CookieManager\Model\Language::class,
        \OxidEsales\Eshop\Core\ViewConfig::class =>
            \Netensio\CookieManager\Model\ViewConfig::class,
        \OxidEsales\Eshop\Application\Component\Widget\CookieNote::class =>
            \Netensio\CookieManager\Model\CookieNote::class,
    ],
    'controllers' => [
        'cookiemanager' =>
            \Netensio\CookieManager\Controller\Admin\CookieManagerController::class,
        'cookiemanager_list' =>
            \Netensio\CookieManager\Controller\Admin\CookieManagerListController::class,
        'cookiemanager_main' =>
            \Netensio\CookieManager\Controller\Admin\CookieManagerMainController::class,
        'net_cookie_manager_frontend_controller' =>
            \Netensio\CookieManager\Controller\CookieManagerFrontendController::class,
    ],
    'events' => [
        'onActivate' => '\Netensio\CookieManager\Core\Events::onActivate',
        'onDeactivate' => '\Netensio\CookieManager\Core\Events::onDeactivate',
    ],
    'templates' => [
        'cookiemanager.tpl' =>
            'netensio/net_cookie_manager/views/admin/tpl/cookiemanager.tpl',
        'cookiemanager_list.tpl' =>
            'netensio/net_cookie_manager/views/admin/tpl/cookiemanager_list.tpl',
        'cookiemanager_main.tpl' =>
            'netensio/net_cookie_manager/views/admin/tpl/cookiemanager_main.tpl',
        'widget/header/net_cookie_manager_cookienote.tpl' =>
            'netensio/net_cookie_manager/views/tpl/widget/header/net_cookie_manager_cookienote.tpl',
    ],
    'blocks' => [
        [
            'template' => 'layout/base.tpl',
            'block' => 'base_style',
            'file' => '/views/blocks/net_cookie_manager_base_style.tpl'
        ],
    ],
];