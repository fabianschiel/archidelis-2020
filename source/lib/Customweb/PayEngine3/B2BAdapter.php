<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

require_once 'Customweb/Form/Element.php';
require_once 'Customweb/Form/Validator/NotEmpty.php';
require_once 'Customweb/I18n/Translation.php';
require_once 'Customweb/Form/Control/TextInput.php';
require_once 'Customweb/PayEngine3/Adapter.php';


/**
 * @author Sebastian Bossert
 * @Bean
 */
class Customweb_PayEngine3_B2BAdapter extends Customweb_PayEngine3_Adapter {

	const CUSTOMER_TYPE = 'M';

	/**
	 * @param \Customweb_Payment_Authorization_IOrderContext $orderContext
	 *
	 * @return bool
	 */
	public function isB2B(Customweb_Payment_Authorization_IOrderContext $orderContext){
		$company = $orderContext->getBillingAddress()->getCompanyName();
		return !empty($company) || $orderContext->getBillingAddress()->getGender() == 'company';
	}

	/**
	 * @param \Customweb_PayEngine3_Authorization_Transaction $transaction
	 *
	 * @return array
	 */
	public function getPayload(Customweb_PayEngine3_Authorization_Transaction $transaction)
	{
		return [
			'customerType' => 'organization',
			'companyName'  => $this->getCompanyName($transaction),
		];
	}

	/**
	 * @param \Customweb_Payment_Authorization_IOrderContext           $orderContext
	 * @param \Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getVisibleFormFields(
		Customweb_Payment_Authorization_IOrderContext $orderContext,
		Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext
	){
		$fields = [];
		$previous = $paymentContext->getMap();
		$companyName = $orderContext->getBillingAddress()->getCompanyName();
		if (empty($companyName)) {
			$fields[] = $this->getTextControl(
				'payengine3-company-name',
				Customweb_I18n_Translation::__("Company Name"),
				$previous
			);
		}
		return $fields;
	}

	/**
	 * @param \Customweb_PayEngine3_Authorization_Transaction $transaction
	 *
	 * @return string|null
	 */
	public function getCompanyName(Customweb_PayEngine3_Authorization_Transaction $transaction){
		$value = $transaction->getTransactionContext()->getOrderContext()->getBillingAddress()->getCompanyName();
		if (empty($value)) {
			$value = $transaction->getFormDataByIndex('payengine3-company-name');
		}
		return $value;
	}

	/**
	 * @param string $name
	 * @param string $label
	 * @param array $previous
	 * @param bool  $required
	 *
	 * @return \Customweb_Form_Element
	 * @throws \Exception
	 */
	private function getTextControl($name, $label, array $previous, $required = true){
		$default = '';
		if (isset($previous[$name])) {
			$default = $previous[$name];
		}
		$control = new Customweb_Form_Control_TextInput($name, $default);
		if ($required) {
			$control->addValidator(
					new Customweb_Form_Validator_NotEmpty(
						$control,
						Customweb_I18n_Translation::__(
							"Please enter a value for '@label'.",
							['@label' => $label]
						)
					)
			);
		}
		$control->setRequired($required);
		$element = new Customweb_Form_Element($label, $control);
		$element->setRequired($required);
		return $element;
	}
}
