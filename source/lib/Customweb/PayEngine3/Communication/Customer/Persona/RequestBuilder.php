<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

require_once 'Customweb/Core/DateTime.php';
require_once 'Customweb/PayEngine3/Communication/AbstractTransactionRequestBuilder.php';
require_once 'Customweb/PayEngine3/Util.php';



/**
 *
 * @author Sebastian Bossert
 */
class Customweb_PayEngine3_Communication_Customer_Persona_RequestBuilder extends Customweb_PayEngine3_Communication_AbstractTransactionRequestBuilder {
	
	private $customerId;
	private $formData;
	private $orderContext;

	public function __construct(Customweb_DependencyInjection_IContainer $container, Customweb_PayEngine3_Authorization_Transaction $transaction, array $formData, $customerId){
		parent::__construct($container, $transaction);
		$this->formData = $formData;
		$this->customerId = $customerId;
		$this->orderContext = $this->getTransaction()->getTransactionContext()->getOrderContext();
	}

	protected function getMethod(){
		return 'POST';
	}

	protected function getPayload(){
		// @formatter:off
		$parameters = array_merge(
				$this->getMandatoryParameter('firstName', Customweb_PayEngine3_Util::cleanStringAlphaExtended($this->getAddress()->getFirstName(), 35)),
				$this->getMandatoryParameter('lastName', Customweb_PayEngine3_Util::cleanStringAlphaExtended($this->getAddress()->getLastName(), 35)), 
				$this->getOptionalParameter('mobile', Customweb_PayEngine3_Util::cleanPhoneNumber($this->getAddress()->getMobilePhoneNumber())),
				$this->getOptionalParameter('phone', Customweb_PayEngine3_Util::cleanPhoneNumber($this->getPhoneNumberOrPlaceholder())),
				$this->getOptionalParameter('title', Customweb_PayEngine3_Util::cleanStringAlphaExtended($this->getAddress()->getSalutation(), 35))
		);
		
		// @formatter:on
		$parameters = array_replace_recursive($parameters, $this->getPaymentMethod()->getAdditionalPersonaPayload($this->getTransaction(),$this->formData));

		// only set gender for ratepay b2c customers
		if (
			strpos($this->getTransaction()->getPaymentMethod()->getPaymentMethodName(), 'ratepay') !== false
			&& !$this->getContainer()->getB2BAdapter()->isB2B($this->orderContext)
		) {
			$parameters = array_merge($parameters, $this->getGenderParameter(), $this->getBirthdayParameter());
		}

		return $parameters;

	}

	private function getBirthdayParameter(){
		$birthday = $this->getAddress()->getDateOfBirth();

		if (empty($birthday)) {
			$year = trim($this->formData['payengine3-y']);
			$month = trim($this->formData['payengine3-m']);
			$day = trim($this->formData['payengine3-d']);

			if (!empty($year) && !empty($month) && !empty($day)) {
				$birthday = new Customweb_Core_DateTime();
				$birthday->setDate(intval($year), intval($month), intval($day));
			}
		}

		if ($birthday instanceof DateTime) {
			$birthday->setTime(0,0,0); // ensure equal timestamp
			$birthday->setTimezone(new DateTimeZone('UTC'));
			return array(
				'birthday' => $birthday->format('U')*1000
			); 
		}
		return array();
	}

	private function getGenderParameter(){
		$gender = $this->getAddress()->getGender();
		
		if (empty($gender)) {
			$gender = $this->formData['payengine3-gender'];
		}

		if ($gender == 'male') {
			return array(
				'gender' => 'MALE' 
			);
		}
		else if ($gender == 'female') {
			return array(
				'gender' => 'FEMALE' 
			);
		}
		return array();
	}

	protected function getUrlPath(){
		return '/customers/' . $this->customerId . '/personas';
	}

	private function getAddress(){
		return $this->orderContext->getBillingAddress();
	}

	protected function getPhoneNumberOrPlaceholder() {
		$phone = $this->getAddress()->getPhoneNumber();
		
		if (empty($phone) && isset($this->formData['payengine3-phone'])) {
			$phone = $this->formData['payengine3-phone'];
		}

		// check if payment method is a Ratepay method
		if (strpos($this->getTransaction()->getPaymentMethod()->getPaymentMethodName(), 'ratepay') !== false) {
			// set placeholder if phone number is empty
			return empty($phone) ? '030 33988560' : $phone;
		}

		return $phone;
	}
}