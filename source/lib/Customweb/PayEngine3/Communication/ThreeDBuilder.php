<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

require_once 'Customweb/Util/Currency.php';
require_once 'Customweb/PayEngine3/Adapter.php';


/**
 *
 * @author Sebastian Bossert
 */
class Customweb_PayEngine3_Communication_ThreeDBuilder extends Customweb_PayEngine3_Adapter {
	private $transaction;

	public function __construct(Customweb_PayEngine3_Authorization_Transaction $transaction, Customweb_DependencyInjection_IContainer $container){
		parent::__construct($container);
		$this->transaction = $transaction;
	}

	public function getJson(){
		return json_encode($this->getParameters());
	}

	public function getParameters(){
		return array(
			'type' => $this->getType(),
			'transactionData' => $this->getTransactionData(),
			'async' => $this->getUrls()
		);
	}

	private function getTransactionData(){
		$transactionData = array(
			'amount' => intval(
					Customweb_Util_Currency::formatAmount($this->getOrderContext()->getOrderAmountInDecimals(),
							$this->getOrderContext()->getCurrencyCode(), '')),
			'currency' => $this->getOrderContext()->getCurrencyCode()
		);
		if ($this->getTransaction()->getTransactionContext()->createRecurringAlias()) {
			$transactionData['recurringExpiry'] = '2030-12-12';
			$transactionData['recurringFrequency'] = 1;
		}
		
		return $transactionData;
	}

	private function getType(){
		return $this->getTransaction()->getTransactionContext()->createRecurringAlias() ? 'RECURRING_TRANSACTION' : 'PAYMENT_TRANSACTION';
	}

	private function getUrls(){
		return array(
			'successUrl' => $this->getTransaction()->getSuccessUrl(),
			'failureUrl' => $this->getTransaction()->getFailedUrl(),
			'notificationUrl' => $this->getReturnUrl()
		);
	}

	private function getReturnUrl(){
		return $this->getContainer()->createSecuredEndpointUrl('process', 'notify3d', $this->getTransaction());
	}

	private function getTransaction(){
		return $this->transaction;
	}

	private function getOrderContext(){
		return $this->getTransaction()->getTransactionContext()->getOrderContext();
	}
}