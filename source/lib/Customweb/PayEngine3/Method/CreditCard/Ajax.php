<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

require_once 'Customweb/PayEngine3/Authorization/Transaction.php';
require_once 'Customweb/Form/Element.php';
require_once 'Customweb/PayEngine3/Method/CreditCard/Validator/Ajax.php';
require_once 'Customweb/I18n/Translation.php';
require_once 'Customweb/PayEngine3/Method/Default.php';
require_once 'Customweb/Form/Control/Html.php';
require_once 'Customweb/PayEngine3/Communication/ThreeDBuilder.php';
require_once 'Customweb/Form/WideElement.php';
require_once 'Customweb/Payment/Util.php';
require_once 'Customweb/Util/JavaScript.php';


/**
 *
 * @author Sebastian Bossert
 * @Method(paymentMethods={'creditcard'}, authorizationMethods={'ajaxauthorization', 'recurring'})
 */
class Customweb_PayEngine3_Method_CreditCard_Ajax extends Customweb_PayEngine3_Method_Default {

	/**
	 * Returns prefix to uniquely identify the widget's payment method.
	 */
	public function getWidgetPrefix(){
		return 'payengine3_' . $this->getPaymentMethodName() . '_';
	}

	public function getOrderId(Customweb_PayEngine3_Authorization_Transaction $transaction){
		$applied = Customweb_Payment_Util::applyOrderSchemaImproved($this->getContainer()->getConfiguration()->getOrderIdSchema(),
				$transaction->getExternalTransactionId(), 30);
		$applied = str_replace("_", "x", $applied); // separate externaltransactionid
		$applied = preg_replace("/\W/", "", $applied);
		return $applied;
	}

	protected function getAliasCallbackFunction(Customweb_PayEngine3_Authorization_Transaction $transaction, Customweb_PayEngine3_Authorization_Transaction $alias){
		return "function(formFieldValues){
	window.PayEngine.verifyPaymentInstrument({$alias->getPaymentInstrumentId()}, formFieldValues['payengine3-cvc']
}";
	}

	/**
	 * Returns submission url with single quotation marks surrounding it, and containing reference to tracking field if required.
	 *
	 * @param Customweb_PayEngine3_Authorization_Transaction $transaction
	 * @param string $action
	 * @return string
	 */
	private function getSubmissionUrl(Customweb_PayEngine3_Authorization_Transaction $transaction, $action){
		$url = "'" . $this->getContainer()->createSecuredEndpointUrl('process', $action, $transaction);
		if ($this->useDeviceId($transaction->getTransactionContext()->getOrderContext())) {
			$url .= "&payengine3-tracking=' + formFieldValues['payengine3-tracking']";
		}
		else {
			$url .= "'";
		}
		return $url;
	}

	public function getJavaScriptCallbackFunction(Customweb_PayEngine3_Authorization_Transaction $transaction){
		$context = $transaction->getTransactionContext();
		/* @var $context Customweb_Payment_Authorization_Ajax_ITransactionContext */

		$successCallback = $context->getJavaScriptSuccessCallbackFunction();

// 		if ($transaction->getTransactionContext()->getAlias() instanceof Customweb_PayEngine3_Authorization_Transaction) {
// 			$url = $this->getSubmissionUrl($transaction, 'alias');
// 			return "function(formFieldValues){($successCallback)($url);}";
// 		}

		$failCallback = $context->getJavaScriptFailedCallbackFunction();

		$submitUrl = $this->getSubmissionUrl($transaction, 'return');
		$failUrl = "'" . $this->getContainer()->createSecuredEndpointUrl('process', 'fail', $transaction) . "'";
		$prefix = $this->getWidgetPrefix();

		$threeDBuilder = new Customweb_PayEngine3_Communication_ThreeDBuilder($transaction, $this->getContainer());
		$threeDParams = $threeDBuilder->getJson();

		$getOverlayFunction = $this->getOverlayScript();

		return "function(formFieldValues) {
	document.{$prefix}Success = {url: {$submitUrl}, callback: ({$successCallback})};
	document.{$prefix}Fail = {url: {$failUrl}, callback: ({$failCallback})};

	let element = ($getOverlayFunction)();
	
	document.{$prefix}ThreeD = function(error, result) {
		window.PayEngine.threeDsAuthentication(result.paymentInstrumentId, element, {$threeDParams}, document.{$prefix}ThreeDCallback);
	};
	
	window.PayEngineWidget.pay(document.{$prefix}Reference);
}";
	}

	protected function getOverlayScript(){
		// only used locally, does not need to be defined on document
		return "function getOverlay() {
	var overlay = document.getElementById('payengine3-overlay');
	if(overlay === null) {
		overlay = document.createElement('div');
		overlay.id = 'payengine3-overlay';
		overlay.style = 'display: block; z-index: 9999; background-color: rgba(0,0,0,0.4); position:fixed; left:0; top:0; width:100%; height:100%';
		var container = document.createElement('div');
		container.style = 'position:relative; width:70%; height:70%; margin:auto; margin-top: 3em;';
		overlay.appendChild(container);
		document.getElementsByTagName('body')[0].appendChild(overlay);
	}
	return overlay.querySelector('div');
}";
	}

	protected function getSelectedAliasField(Customweb_PayEngine3_Authorization_Transaction $transaction){
		$control = new Customweb_Form_Control_Html('payengine3-selected', $transaction->getAliasForDisplay());
		$control->setRequired(false);
		$field = new Customweb_Form_Element(Customweb_I18n_Translation::__("Selected payment"), $control);
		$field->setRequired(false);
		return $field;
	}

	protected function getAliasFields(Customweb_PayEngine3_Authorization_Transaction $transaction){
		$fields = parent::getVisibleFormFields($transaction->getTransactionContext()->getOrderContext(), $transaction->getAlias(), null,
				$transaction->getTransactionContext()->getPaymentCustomerContext());

		$control = new Customweb_Form_Control_Html('payengine3-selected', $transaction->getAliasForDisplay());
		$fields[] = new Customweb_Form_Element(Customweb_I18n_Translation::__("Selected payment"), $control);

		return $fields;
	}

	public function getVisibleFormFields(Customweb_Payment_Authorization_IOrderContext $orderContext, $aliasTransaction, $failedTransaction, Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext){
		$fields = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext);
		if (!$this->useDeviceId($orderContext)) {
			$fields[] = $this->getLoadingElement($orderContext, $paymentCustomerContext);
		}

		$fields[] = $this->getWidgetFormField($orderContext, $aliasTransaction);

		return $fields;
	}

	protected function getWidgetScriptError(){
		$prefix = $this->getWidgetPrefix();
		return "
		document.{$prefix}InvokeFail = function(error) {
			if(error.errors) {
				var codes = error.errors.map(function(e) { return e.code; }).join(',');
				var messages = error.errors.map(function(e) { return e.message; }).join(' ');
				document.{$prefix}Fail.url += '&codes=' + codes + '&messages=' + messages;
			}
			document.{$prefix}Fail.url += '&message=' + error.message;
			document.{$prefix}Error = error.message;
			document.{$prefix}Fail.callback(document.{$prefix}Fail.url);
		}";
	}

	protected function getWidgetScriptResultHandling(){
		$prefix = $this->getWidgetPrefix();
		$getOverlayFunction = $this->getOverlayScript();
		$threedFailedError = "{message: '" . Customweb_I18n_Translation::__("The 3D-Secure Authentication process failed.")->toString() . "'}";
		return "
		document.{$prefix}ResultCallback = function(error, result) {
			if (error && !(error.cardNumber && error.expiry && error.verification)) {
				document.{$prefix}InvokeFail(error);
				return;
			}
			if (typeof document.{$prefix}Success === 'undefined' || typeof document.{$prefix}Fail === 'undefined') {
				alert('The payment could not be loaded. Please reload the page and try again.');
				return;
			}
			else {
				document.{$prefix}SetPaymentInstrument(result.paymentInstrumentId);
				document.{$prefix}ThreeD(error, result);
			}
		}
		
		document.{$prefix}InitResultObj = function() {
			if(typeof document.{$prefix} === 'undefined') {
				document.{$prefix} = {};
			}
		}
		
		document.{$prefix}SetPaymentInstrument = function(paymentInstrument) {
			document.{$prefix}InitResultObj();
			document.{$prefix}.paymentInstrument = paymentInstrument;
		}
		
		document.{$prefix}SetAuthenticationId = function(authenticationId) {
			document.{$prefix}InitResultObj();
			document.{$prefix}.authenticationId = authenticationId;
		}

		document.{$prefix}SetThreeDVersionId = function(versionId) {
			document.{$prefix}InitResultObj();
			document.{$prefix}.threeDVersionId = versionId;
		}
		
		document.{$prefix}InvokeSuccess = function() {
			var url = document.{$prefix}Success.url;
			if(document.{$prefix}.paymentInstrument) {
				url += '&paymentInstrumentId=' + document.{$prefix}.paymentInstrument;
			}
			if(document.{$prefix}.authenticationId) {
				url += '&authenticationId=' + document.{$prefix}.authenticationId;
			}
			if(document.{$prefix}.threeDVersionId) {
				url += '&threeDVersionId=' + document.{$prefix}.threeDVersionId;
			}
			document.{$prefix}Success.callback(url);
		}

		document.{$prefix}ThreeDCallback = function(error, result) {
			if(error && !(error.cardNumber && error.expiry && error.verification)) {
				document.{$prefix}InvokeFail(error);
				return;
			}
			else {
				if(result.specificData.status === 'N') {
					document.{$prefix}SetAuthenticationId(result.specificData.authenticationId);
					document.{$prefix}InvokeSuccess();
				}
				else {
					if(result.threeDsVersion === '1.0') {
						document.{$prefix}SetThreeDVersionId(result.specificData.threeDsVersionId);
						document.{$prefix}InvokeSuccess();
					}
					else if(result.threeDsVersion === '2.0') {
						document.{$prefix}SetAuthenticationId(result.specificData.authenticationId);
						document.{$prefix}InvokeSuccess();
					}
					else {
						document.{$prefix}InvokeSuccess();
					}
				}
			}
		}";
	}

	protected function getWidgetScriptInitialization(Customweb_Payment_Authorization_IOrderContext $orderContext, $alias){
		$prefix = $this->getWidgetPrefix();
		$loadJs = Customweb_Util_JavaScript::loadScript($this->getContainer()->getConfiguration()->getWidgetJavascriptUrl(),
				'payengine3WidgetLoaded', $prefix . 'WidgetInitialize');

		// CHANGES FOR UPDATE:
		// - initAsInlineComponentPi => initAsInlineComponentPiUpdate
		// - less optional parameters (only language, customStyle, hidePayButton
		// - add paymentInstrumentId parameter after merchantId, before optionalParameters

		return "
		var {$prefix}Reference;
		document.{$prefix}InitCallback = function(error, result) {
			if(error && !(error.cardNumber && error.expiry && error.verification)){
				document.{$prefix}InvokeFail(error);
				return;
			}
			document.{$prefix}Reference = result;
		}
		
		function {$prefix}WidgetInitialize(){
			var container = document.getElementById('{$prefix}widget-container');
			container.parentNode.style.display = 'block';
			{$this->getWidgetScriptSetOptionalParameters($orderContext, $alias)}
			
			document.{$prefix} = { parameters: optionalParameters, container: container };
			{$this->getWidgetScriptInitCall($alias)}
		}
		if (typeof window.payengine3WidgetLoadedSafariSafe === 'undefined') {
			window.payengine3WidgetLoadedSafariSafe = 'defined';
			function payengine3WidgetLoaded(){
				return (typeof PayEngineWidget !== 'undefined' && typeof PayEngine !== 'undefined');
			}
			{$loadJs}
		}
		else {
			{$prefix}WidgetInitialize();
		}";
	}

	public function getAdditionalOrderPayload(Customweb_PayEngine3_Authorization_Transaction $transaction, array $formData){
		$payload = parent::getAdditionalOrderPayload($transaction, $formData);
		$authenticationId = $transaction->getAuthenticationId();
		if (isset($formData['authenticationId'])) {
			$authenticationId = $formData['authenticationId'];
		}
		$versionId = $transaction->getThreeDVersionId();
		if (isset($formData['threeDVersionId'])) {
			$versionId = $formData['threeDVersionId'];
		}
		if ($authenticationId) {
			$payload['meta']['threeDsData']['threeDsAuthenticationId'] = $authenticationId;
		}
		if ($versionId) {
			$payload['meta']['threeDsData']['threeDsVersionId'] = $versionId;
		}
		$alias = $transaction->getTransactionContext()->getAlias();
		if ($alias === 'new') {
			if (!isset($payload['payment'])) {
				$payload['payment'] = array();
			}
			$payload['payment']['cofContract'] = array(
				'type' => 'ONECLICK'
			);
		}
		else if ($alias instanceof Customweb_PayEngine3_Authorization_Transaction) {
			$pi = $alias->getPaymentInstrumentAttributes();
			if (isset($pi['cofContracts']) && count($pi['cofContracts']) > 0) {
				if (!isset($payload['payment'])) {
					$payload['payment'] = array();
				}
				$payload['payment']['cofContract'] = array(
					'id' => $pi['cofContracts'][0]['id']
				);
			}
		}
		return $payload;
	}

	protected function getWidgetScriptInitCall($alias){
		$prefix = $this->getWidgetPrefix();
		if ($alias instanceof Customweb_PayEngine3_Authorization_Transaction) {
			return "PayEngineWidget.initAsInlineComponentPiUpdate(
					container,
					'{$this->getContainer()->getConfiguration()->getMerchantId()}',
					'{$alias->getPaymentInstrumentId()}',
					optionalParameters,
					document.{$prefix}ResultCallback
			);";
		}
		return "PayEngineWidget.initAsInlineComponentPi(
					container,
					'{$this->getContainer()->getConfiguration()->getMerchantId()}',
					optionalParameters,
					document.{$prefix}ResultCallback
			);";
	}

	protected function getWidgetScriptSetOptionalParameters(Customweb_Payment_Authorization_IOrderContext $orderContext, $alias){
		$prefix = $this->getWidgetPrefix();
		$customStyle = '';
		if ($this->existsPaymentMethodConfigurationValue('custom_style')) {
			$customStyle = $this->getPaymentMethodConfigurationValue('custom_style');
		}
		$parameters = "
			var optionalParameters = new Object;
			optionalParameters.initCallback = document.{$prefix}InitCallback;
			optionalParameters.language = '{$orderContext->getLanguage()->getIso2LetterCode()}';
			optionalParameters.customStyleId = '{$customStyle}' || null;
			optionalParameters.hidePayButton = true;";
		if (!($alias instanceof Customweb_PayEngine3_Authorization_Transaction)) {
			$parameters .= "
			optionalParameters.layout = null;
			optionalParameters.products = ['{$this->getProductName()}'];
			optionalParameters.redirectUser = true;
			optionalParameters.hideTitleIcons = true;
			optionalParameters.skipFinalResultView = true;
			optionalParameters.creditCard = {'cardHolder': false };";

			if ($alias === 'new') {
				$parameters .= "
			optionalParameters.creditCard.cofContract = {'type': 'ONECLICK'};";
			}
		}
		return $parameters;
	}

	protected function getWidgetScript(Customweb_Payment_Authorization_IOrderContext $orderContext, $alias){
		return "
		{$this->getWidgetScriptError()}
		{$this->getWidgetScriptResultHandling()}
		{$this->getWidgetScriptInitialization($orderContext, $alias)}";
	}

	protected function getWidgetFormField(Customweb_Payment_Authorization_IOrderContext $orderContext, $alias){
		$prefix = $this->getWidgetPrefix();
		$height = $this->getIframeHeight();
		if (ctype_digit($height) || is_int($height)) {
			$height .= 'em';
		}
		$htmlContent = "<div id='{$prefix}widget-container' style='height:{$height}; overflow:hidden;'></div>";
		$control = new Customweb_Form_Control_Html('payengine3-container', $htmlContent);
		$control->addValidator($this->getValidator($control));

		if ($this->useWideElement()) {
			$field = new Customweb_Form_WideElement($control);
		}
		else {
			$field = new Customweb_Form_Element('', $control);
		}
		$field->setRequired(false);
		$field->appendJavaScript($this->getWidgetScript($orderContext, $alias));
		return $field;
	}

	protected function getValidator(Customweb_Form_Control_IControl $control){
		return new Customweb_PayEngine3_Method_CreditCard_Validator_Ajax($control, $this->getWidgetPrefix());
	}

	private function useWideElement(){
		return $this->existsPaymentMethodConfigurationValue('element_style') && $this->getPaymentMethodConfigurationValue('element_style') === 'wide';
	}

	protected function getIframeHeight(){
		if ($this->existsPaymentMethodConfigurationValue('iframe_height')) {
			$height = $this->getPaymentMethodConfigurationValue('iframe_height');
			if (!empty($height)) {
				return $height;
			}
		}
		return 15;
	}

	public function setAliasForDisplay(Customweb_PayEngine3_Authorization_Transaction $transaction, array $paymentInstrument){
		if ($transaction->getTransactionContext()->getAlias() == 'new' && isset($paymentInstrument['recurring']) && $paymentInstrument['recurring'] &&
				isset($paymentInstrument['attributes']) && isset($paymentInstrument['attributes']['cardNumber'])) {
			$transaction->setAliasForDisplay($paymentInstrument['attributes']['cardNumber']);
		}
	}
}
