<?php

/**
 *  * You are allowed to use this API in your web application.
 *
 * Copyright (C) 2018 by customweb GmbH
 *
 * This program is licenced under the customweb software licence. With the
 * purchase or the installation of the software in your application you
 * accept the licence agreement. The allowed usage is outlined in the
 * customweb software licence which can be found under
 * http://www.sellxed.com/en/software-license-agreement
 *
 * Any modification or distribution is strictly forbidden. The license
 * grants you the installation in one application. For multiuse you will need
 * to purchase further licences at http://www.sellxed.com/shop.
 *
 * See the customweb software licence agreement for more details.
 *
 */

require_once 'Customweb/PayEngine3/Method/RatePay/Invoice.php';
require_once 'Customweb/Form/Element.php';
require_once 'Customweb/Form/Validator/NotEmpty.php';
require_once 'Customweb/Form/Validator/Checked.php';
require_once 'Customweb/I18n/Translation.php';
require_once 'Customweb/Form/Control/Html.php';
require_once 'Customweb/Form/Control/SingleCheckbox.php';
require_once 'Customweb/Form/Control/TextInput.php';


/**
 *
 * @author Sebastian Bossert
 * @Method(paymentMethods={'RatePayDirectDebits'})
 */
class Customweb_PayEngine3_Method_RatePay_DirectDebit extends Customweb_PayEngine3_Method_RatePay_Invoice {

	public function useBasket(){
		return true;
	}

	public static function extractIban(Customweb_Payment_Authorization_IPaymentCustomerContext $paymentContext, array $formData){
		$update = $paymentContext->getMap();
		$iban = null;
		
		if (isset($formData['payengine3-iban'])) {
			$update['payengine3-iban'] = $iban = trim($formData['payengine3-iban']);
		}
		
		if (empty($iban)) {
			throw new Exception(Customweb_I18n_Translation::__("Please enter your IBAN."));
		}
		$paymentContext->updateMap($update);
		return $iban;
	}

	/**
	 * @param \Customweb_PayEngine3_Authorization_Transaction $transaction
	 * @param array                                                    $formData
	 *
	 * @return array
	 * @throws \Customweb_Payment_Exception_PaymentErrorException
	 */
	public function getAdditionalOrderPayload(Customweb_PayEngine3_Authorization_Transaction $transaction, array $formData)
	{
		$additional                             = parent::getAdditionalOrderPayload($transaction, $formData);
		$iban                                   = $this->extractIban($transaction->getPaymentCustomerContext(), $formData);
		$additional['payment']['iban']          = $iban;
		$additional['payment']['accountHolder'] = $this->getAccountHolder($transaction);
		return $additional;
	}

	/**
	 * @param \Customweb_Payment_Authorization_IOrderContext           $orderContext
	 * @param                                                          $aliasTransaction
	 * @param                                                          $failedTransaction
	 * @param \Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext
	 *
	 * @return array|\Customweb_Form_Element|\Customweb_Form_IElement[]
	 * @throws \Exception
	 */
	public function getVisibleFormFields(
		Customweb_Payment_Authorization_IOrderContext $orderContext,
		$aliasTransaction,
		$failedTransaction,
		Customweb_Payment_Authorization_IPaymentCustomerContext $paymentCustomerContext
	){
		$fields = parent::getVisibleFormFields($orderContext, $aliasTransaction, $failedTransaction, $paymentCustomerContext);
		
		// ensure agreement is at the bottom
		$agreement     = array_pop($fields);
		$map           = $paymentCustomerContext->getMap();
		$defaultValues = ['payengine3-iban' => null];
		$defaultValues = array_merge($defaultValues, $map);
		
		$mandateCompanyControl = new Customweb_Form_Control_Html('payengine3-mandate-company', $this->getPaymentMethodParameter('mandate_company'));
		$mandateCompanyControl->setRequired(false);
		$element = new Customweb_Form_Element('', $mandateCompanyControl);
		$element->setRequired(false);
		$fields[] = $element;
		
		$mandateCreditorControl = new Customweb_Form_Control_Html('payengine3-mandate-creditor', $this->getPaymentMethodParameter('mandate_creditor'));
		$mandateCreditorControl->setRequired(false);
		$element = new Customweb_Form_Element(Customweb_I18n_Translation::__('Creditor ID'), $mandateCreditorControl);
		$element->setRequired(false);
		$fields[] = $element;
		
		$legalControl = new Customweb_Form_Control_Html('payengine3-legal-text', $this->getLegalText());
		$legalControl->setRequired(false);
		$element = new Customweb_Form_Element('', $legalControl);
		$element->setRequired(false);
		$fields[] = $element;
		
		$mandateReferenceControl = new Customweb_Form_Control_Html('payengine3-mandate-reference', Customweb_I18n_Translation::__("The mandate reference will be transmitted after order completion."));
		$mandateReferenceControl->setRequired(false);
		$element = new Customweb_Form_Element(Customweb_I18n_Translation::__('Mandate Reference'), $mandateReferenceControl);
		$element->setRequired(false);
		$fields[] = $element;
		
		$holderControl = new Customweb_Form_Control_Html('payengine3-account-holder', $this->getDisplayAccountHolder($orderContext));
		$fields[] = new Customweb_Form_Element(Customweb_I18n_Translation::__('Account Holder'), $holderControl);
		
		$ibanControl = new Customweb_Form_Control_TextInput('payengine3-iban', $defaultValues['payengine3-iban']);
		$ibanControl->addValidator(new Customweb_Form_Validator_NotEmpty($ibanControl, Customweb_I18n_Translation::__('Please enter your IBAN.')));
		$fields[] = new Customweb_Form_Element(Customweb_I18n_Translation::__("IBAN"), $ibanControl);
		
		$mandateTextControl = new Customweb_Form_Control_Html('payengine3-mandate-text', $this->getMandateText());
		$mandateTextControl->setRequired(false);
		$element = new Customweb_Form_Element('', $mandateTextControl);
		$element->setRequired(false);
		$fields[] = $element;
		
		$fields[] = $agreement;
		
		return $fields;
	}

	protected function getAgreementElement(Customweb_Payment_Authorization_IOrderContext $orderContext){
		$textToAgree = Customweb_I18n_Translation::__('I agree to the displayed mandate and linked conditions.');
		$checkbox = new Customweb_Form_Control_SingleCheckbox("payengine3-consent", "accepted", $textToAgree);
		$checkbox->addValidator(new Customweb_Form_Validator_Checked($checkbox, Customweb_I18n_Translation::__('You have to agree to the RatePay agreement.')));
		return new Customweb_Form_Element(Customweb_I18n_Translation::__("Terms &amp; Conditions and Privacy"), $checkbox);
	}

	private function getLegalText(){
		return Customweb_I18n_Translation::__(
			"With clicking on 'buy now' you agree to the <a href='https://www.ratepay.com/legal-payment-terms' title='Legal - Ratepay' target='_blank'>Ratepay Terms of Payment</a> as well as to the performance of a <a href='https://www.ratepay.com/legal-payment-dataprivacy' title='Legal - Ratepay' target='_blank'>risk check by Ratepay</a>.");
	}

	private function getMandateText(){
		return Customweb_I18n_Translation::__(
				"I hereby authorise Ratepay GmbH, Franklinstr. 28-29, 10587 Berlin, Germany, to send instructions to my credit institute to debit all payments arising from this contract to the above-stated bank account, and authorise my credit institute to debit my account with all payments arising from this contract in accordance with the instructions from Ratepay GmbH.<br/>Note:<br/>Upon the conclusion of the contract, I will receive the mandate reference of Ratepay. I may request a refund of the debited amount within eight weeks after the debit date. The Terms & Conditions of my credit institute apply.");
	}

	protected function getPaymentMethodParameter($key){
		$parameters = $this->getPaymentMethodParameters();
		return $parameters[$key];
	}

	public function setAliasForDisplay(Customweb_PayEngine3_Authorization_Transaction $transaction, array $paymentInstrument){
		if ($transaction->getTransactionContext()->getAlias() == 'new' && isset($paymentInstrument['recurring']) && $paymentInstrument['recurring'] &&
				 ($paymentInstrument['attributes']) && isset($paymentInstrument['attributes']['iban'])) {
			$transaction->setAliasForDisplay($paymentInstrument['attributes']['iban']);
		}
	}
}