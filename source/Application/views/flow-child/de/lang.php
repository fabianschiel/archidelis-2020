<?php
/**
 * This file is part of OXID eSales Flow theme.
 *
 * OXID eSales Flow theme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales Flow theme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales Flow theme.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 */

$sLangName = "Deutsch";

$aLang = array(
	'charset' => 'UTF-8',

	// Global
	'DD_SORT_DESC' => 'absteigend',
	'DD_SORT_ASC' => 'aufsteigend',
	'DD_CLOSE_MODAL' => 'schließen',
	'DD_DEMO_ADMIN_TOOL' => 'Admin-Tool starten',
	'DD_DELETE' => 'Löschen',
	'JUST_ARRIVED' => 'Frisch eingetroffen!',
	'DD_NAVIGATION_MORE' => 'Mehr',
	// Form-Validation
	'DD_FORM_VALIDATION_VALIDEMAIL' => 'Bitte geben Sie eine gültige E-Mail-Adresse ein.',
	'DD_FORM_VALIDATION_PASSWORDAGAIN' => 'Die Passwörter stimmen nicht überein.',
	'DD_FORM_VALIDATION_NUMBER' => 'Bitte geben Sie eine Zahl ein.',
	'DD_FORM_VALIDATION_INTEGER' => 'Es sind keine Nachkommastellen erlaubt.',
	'DD_FORM_VALIDATION_POSITIVENUMBER' => 'Bitte geben Sie eine positive Zahl ein.',
	'DD_FORM_VALIDATION_NEGATIVENUMBER' => 'Bitte geben Sie eine negative Zahl ein.',
	'DD_FORM_VALIDATION_REQUIRED' => 'Bitte Wert angeben.',
	'DD_FORM_VALIDATION_CHECKONE' => 'Bitte wählen Sie mindestens eine Option.',

	// Header
	'SEARCH_TITLE' => 'Suchbegriff eingeben...',
	'SEARCH_SUBMIT' => 'Suchen',
	'COOKIE_NOTE_CLOSE' => 'Schließen',

	// Sidebar
	'DD_SIDEBAR_CATEGORYTREE' => 'Kategorien',

	// Footer
	'FOOTER_NEWSLETTER_INFO' => 'Die neuesten Produkte und die besten Angebote per E-Mail, damit Ihr nichts mehr verpasst.',

	// Startseite
	'MANUFACTURERSLIDER_SUBHEAD' => 'Wir präsentieren Ihnen hier unsere sorgsam ausgewählten Marken, deren Produkte Sie in unserem Shop finden.',
	'START_BARGAIN_HEADER' => 'Angebote der Woche',
	'START_NEWEST_HEADER' => 'Neu bei Archidelis',
	'START_TOP_PRODUCTS_HEADER' => 'Topseller',
	'START_BARGAIN_SUBHEADER' => 'Sparen Sie bares Geld mit unseren aktuellen Schnäppchen!',
	'START_NEWEST_SUBHEADER' => 'Frischer geht es nicht. Gerade noch in der Kiste und nun schon im Shop.',
	'START_TOP_PRODUCTS_SUBHEADER' => 'Nur %s Produkte, dafür aber die besten, die wir Euch bieten können.',

	// Kontaktformular
	'DD_CONTACT_PAGE_HEADING' => 'Kontaktieren Sie uns!',
	'DD_CONTACT_FORM_HEADING' => 'Kontaktformular',
	'DD_CONTACT_ADDRESS_HEADING' => 'Adresse',
	'DD_CONTACT_THANKYOU1' => "Vielen Dank für Ihre Nachricht an",
	'DD_CONTACT_THANKYOU2' => ".",
	'DD_CONTACT_SELECT_SALUTATION' => 'Bitte auswählen ...',

	// Link-Seite
	'DD_LINKS_NO_ENTRIES' => 'Es sind leider noch keine Links vorhanden.',

	// 404-Seite
	'DD_ERR_404_START_TEXT' => 'Vielleicht finden Sie die von Ihnen gewünschten Informationen über unsere Startseite:',
	'DD_ERR_404_START_BUTTON' => 'Startseite aufrufen',
	'DD_ERR_404_CONTACT_TEXT' => 'Dürfen wir Ihnen direkt behilflich sein?<br>Gerne können Sie uns anrufen oder eine E-Mail schreiben:',
	'DD_ERR_404_CONTACT_BUTTON' => 'zur Kontaktseite',

	// Login
	'DD_LOGIN_ACCOUNT_PANEL_CREATE_TITLE' => 'Konto eröffnen',
	'DD_LOGIN_ACCOUNT_PANEL_CREATE_BODY' => 'Durch Ihre Anmeldung in unserem Shop, werden Sie in der Lage sein schneller durch den Bestellvorgang geführt zu werden. Des Weiteren können Sie mehrere Versandadressen speichern und Bestellungen in Ihrem Konto verfolgen.',
	'DD_LOGIN_ACCOUNT_PANEL_LOGIN_TITLE' => 'Anmelden',

	// Rechnungs- und Lieferadresse
	'DD_USER_BILLING_LABEL_STATE' => 'Bundesland:',
	'DD_USER_SHIPPING_LABEL_STATE' => 'Bundesland:',
	'DD_USER_SHIPPING_SELECT_ADDRESS' => 'auswählen',
	'DD_USER_SHIPPING_ADD_DELIVERY_ADDRESS' => 'neue Adresse hinzufügen',
	'DD_DELETE_SHIPPING_ADDRESS' => 'Lieferadresse löschen',

	// Bestellhistorie
	'DD_ORDER_ORDERDATE' => 'Datum:',

	// Listen
	'DD_LISTLOCATOR_FILTER_ATTRIBUTES' => 'Filter:',
	'DD_LIST_SHOW_MORE' => 'Produkte ansehen...',

	// Lieblingslisten
	'DD_RECOMMENDATION_EDIT_BACK_TO_LIST' => 'zurück zur Übersicht',

	// Downloads
	'DD_DOWNLOADS_DOWNLOAD_TOOLTIP' => 'herunterladen',
	'DD_FILE_ATTRIBUTES_FILESIZE' => 'Dateigröße:',
	'DD_FILE_ATTRIBUTES_OCLOCK' => 'Uhr',
	'DD_FILE_ATTRIBUTES_FILENAME' => 'Dateiname:',

	// Detailseite
	'BACK_TO_OVERVIEW' => 'Zur Übersicht',
	'OF' => 'von',
	'DD_PRODUCTMAIN_STOCKSTATUS' => 'Lagerbestand',
	'DD_RATING_CUSTOMERRATINGS' => 'Kundenmeinungen',
	'PAGE_DETAILS_CUSTOMERS_ALSO_BOUGHT_SUBHEADER' => 'Kunden die sich diesen Artikel gekauft haben, kauften auch folgende Artikel.',
	'WIDGET_PRODUCT_RELATED_PRODUCTS_ACCESSORIES_SUBHEADER' => 'Folgende Artikel passen gut zu diesem Artikel.',
	'WIDGET_PRODUCT_RELATED_PRODUCTS_SIMILAR_SUBHEADER' => 'Schauen Sie sich doch auch unsere ähnlichen Artikel an.',
	'WIDGET_PRODUCT_RELATED_PRODUCTS_CROSSSELING_SUBHEADER' => 'Kunden die sich diesen Artikel angesehen haben, haben sich auch folgende Artikel angesehen.',
	'DETAILS_VPE_MESSAGE_1' => 'Dieser Artikel kann nur in Verpackungseinheiten zu je',
	'DETAILS_VPE_MESSAGE_2' => 'erworben werden.',
	'HAVE_YOU_SEEN' => '...und das passt dazu:',

	// Modal-Warenkorb
	'DD_MINIBASKET_MODAL_TABLE_TITLE' => 'Artikel',
	'DD_MINIBASKET_MODAL_TABLE_PRICE' => 'Gesamtsumme',
	'DD_MINIBASKET_CONTINUE_SHOPPING' => 'weiter einkaufen',

	// Checkout
	'DD_BASKET_BACK_TO_SHOP' => 'zurück zum Shop',

	// E-Mails
	'DD_ORDER_CUST_HEADING' => 'Bestellung',
	'DD_FOOTER_FOLLOW_US' => 'Folgen Sie uns:',
	'DD_FOOTER_CONTACT_INFO' => 'Kontakt:',
	'DD_FORGOT_PASSWORD_HEADING' => 'Passwort vergessen',
	'DD_INVITE_HEADING' => 'Artikel-Empfehlung',
	'DD_INVITE_LINK' => 'Link',
	'DD_NEWSLETTER_OPTIN_HEADING' => 'Ihre Newsletter-Anmeldung',
	'DD_ORDERSHIPPED_HEADING' => 'Versandbestätigung - Bestellung',
	'DD_OWNER_REMINDER_HEADING' => 'Lagerbestand niedrig',
	'DD_PRICEALARM_HEADING' => 'Preisalarm',
	'DD_REGISTER_HEADING' => 'Ihre Registrierung',
	'DD_DOWNLOADLINKS_HEADING' => 'Ihre Downloadlinks - Bestellung',
	'DD_SUGGEST_HEADING' => 'Artikel-Empfehlung',
	'DD_WISHLIST_HEADING' => 'Wunschzettel',

	'DD_ROLES_BEMAIN_UIROOTHEADER' => 'Menü',

	'DD_DELETE_MY_ACCOUNT_WARNING' => 'Dieser Vorgang kann nicht rückgängig gemacht werden. Alle persönlichen Daten werden dauerhaft gelöscht.',
	'DD_DELETE_MY_ACCOUNT' => 'Konto löschen',
	'DD_DELETE_MY_ACCOUNT_CONFIRMATION_QUESTION' => 'Sind Sie sicher, dass Sie Ihr Konto löschen wollen?',
	'DD_DELETE_MY_ACCOUNT_CANCEL' => 'Abbrechen',
	'DD_DELETE_MY_ACCOUNT_SUCCESS' => 'Ihr Konto wurde gelöscht',
	'DD_DELETE_MY_ACCOUNT_ERROR' => 'Das Konto konnte nicht gelöscht werden',

	// Account -> My product reviews
	'DD_DELETE_REVIEW_AND_RATING' => 'Bewertung und Sterne-Rating löschen',
	'DD_REVIEWS_NOT_AVAILABLE' => 'Es liegen keine Bewertungen vor',
	'DD_DELETE_REVIEW_CONFIRMATION_QUESTION' => 'Sind Sie sicher, dass Sie die Bewertung löschen wollen?',

	// Contact page
	'DD_SELECT_SALUTATION' => 'Bitte auswählen',

	'DD_CATEGORY_RESET_BUTTON' => 'Zurücksetzen',

	//Cookie Notice
	'COOKIE_NOTE' => 'Dieser Online-Shop verwendet Cookies für ein optimales Einkaufserlebnis. Dabei werden beispielsweise die Session-Informationen oder die Spracheinstellung auf Ihrem Rechner gespeichert. Ohne Cookies ist der Funktionsumfang des Online-Shops eingeschränkt. Weitere Informationen dazu finden Sie in unserer <a href="/datenschutz">Datenschutzerklärung</a>.',
	'COOKIE_NOTE_NEW' => 'Dieser Online-Shop verwendet Cookies für ein optimales Einkauferlebnis. Dabei werden beispielsweise die Session-Informationen oder die Spracheinstellung auf Ihrem Rechner gespeichert. Ohne Cookies ist der Funktionsumfang des Online-Shops eingeschränkt.',


	//Custom Fields
	'BUY-AT-ARCH' => 'Bei Archidelis einkaufen',
	'SPERRGUT' => 'Für übergroße Sendungen müssen wir einen Zuschlag verrechen - mehr dazu erfahren Sie unter: <a href="/Versandkosten-Zahlung-und-Lieferung">Versand und Kosten</a>',
	'LATESTINFO' => "Aktuelle Information",
	'ERROR_MESSAGE_COMPLETE_FIELDS_CORRECTLY' => 'Felder mit einem * sowie Ihre Kreditkartendaten korrekt müssen ausgefüllt werden.',
	'D1Netto' => 'Nettobetrag',
	'D1NettoRd' => 'Netto ger.',

	// Owner E-Mail Custom Fields
	'D1_Email_BILLING_ADDRESS' => 'Rechnungsadresse',
	'D1_Email_VAT_ID_NUMBER' => 'USt-ID',
	'D1_Email_PHONE' => 'Telefon',
	'D1_Email_SHIPPING_ADDRESS' => 'Lieferadresse',
	'D1_Email_PRODUCT' => 'Artikel',
	'D1_Email_UNIT_PRICE' => 'Einzelpreis',
	'D1_Email_QUANTITY' => 'Menge',
	'D1_Email_VAT' => 'MwSt',
	'D1_Email_TOTAL' => 'Gesamtbetrag',
	'D1_Email_PRODUCT_NO' => 'Art. Nr',
	'D1_Email_DISCOUNT' => 'Rabatt',
	'D1_Email_USED_COUPONS' => 'Folgende Gutscheine werden benutzt',
	'D1_Email_REBATE' => 'Nachlass',
	'D1_Email_TOTAL_NET' => 'Summe Artikel (netto)',
	'D1_Email_TOTAL_GROSS' => 'Summe Artikel (brutto)',
	'D1_Email_SURCHARGE' => 'Aufschlag',
	'D1_Email_COUPON' => 'Gutschein',
	'D1_Email_SHIPPING_NET' => 'Versandkosten (netto)',
	'D1_Email_BASKET_TOTAL_PLUS_PROPORTIONAL_VAT' => 'plus MwSt. (anteilig berechnet)',
	'D1_Email_VAT_PLUS_PERCENT_AMOUNT' => 'zzgl. %s%% MwSt., Betrag',
	'D1_Email_SHIPPING_COST' => 'Versandkosten',
	'D1_Email_PLUS_VAT' => 'zzgl. MwSt.',
	'D1_Email_GRAND_TOTAL' => 'Gesamtbetrag',
	'D1_Email_MESSAGE' => 'Nachricht',
	'D1_Email_PAYMENT_INFORMATION' => 'Bezahlinformation',
	'D1_Email_PAYMENT_METHOD' => 'Zahlungsart',
	'D1_Email_DEDUCTION' => 'Abschlag',
	'D1_Email_EMAIL_ADDRESS' => 'E-Mail-Adresse',
	'D1_Email_SELECTED_SHIPPING_CARRIER' => 'Der Versand erfolgt mit',
	'D1_Email_PRODUCT_REVIEW' => 'Artikel bewerten',
	'D1_Email_WHAT_I_WANTED_TO_SAY' => 'Ihre Mitteilung an uns',
	'D1_Email_MY_DOWNLOADS_DESC' => 'Laden Sie Ihre bestellten Dateien hier herunter.',
	'D1_Email_DOWNLOADS_PAYMENT_PENDING' => 'Die Bezahlung der Bestellung ist noch nicht abgeschlossen.',
	'D1_Email_BANK_DETAILS' => 'Bankdetails',
	'D1_Email_BANK' => 'Bankname',
	'D1_Email_BANK_ACCOUNT_HOLDER' => 'Kontoinhaber',
	'D1_Email_BANK_ACCOUNT_NUMBER' => 'Kontonummer oder IBAN',
	'D1_Email_BANK_CODE' => 'BLZ oder BIC',
	'D1_Email_BIC' => 'BIC',
	'D1_Email_IBAN' => 'IBAN',
	'D1_Email_DD_FOOTER_FOLLOW_US' => 'Folgen Sie uns:',
	'D1_Email_DD_FOOTER_CONTACT_INFO' => 'Kontakt:',

	//Footer Custom
	'SERVICES' => 'SERVICE ',

	//Custom Fields for Contact Form
	'D1_CF_TITLE' => 'Anrede',
	'D1_CF_FIRST_NAME' => 'Vorname',
	'D1_CF_LAST_NAME' => 'Nachname',
	'D1_CF_EMAIL' => 'E-mail',
	'D1_CF_SUBJECT' => 'Betreff',
	'D1_CF_MESSAGE' => 'Nachricht',
	'D1_CF_COMPLETE_MARKED_FIELDS' => 'Bitte alle fett beschrifteten Pflichtfelder ausfüllen.',
	'D1_CF_SEND' => 'Abschicken',
	'D1_CF_DD_CONTACT_SELECT_SALUTATION' => 'Bitte auswählen ...',
	'D1_CF_MRS' => 'Frau',
	'D1_CF_MR' => 'Herr',

	'SHOP_THEME_sEcondaAccountId' => 'econda-ID',
	'HELP_SHOP_THEME_sEcondaAccountId' => 'Please enter your econda ID.<br>Format: XXXXXXXX-XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX-X.<br>Please keep in mind to activate the econda Webshop Controlling in your shop.',


	'NET_COOKIE_MANAGER_MAIN_GROUPNAME_0' => 'Funktional',
	'NET_COOKIE_MANAGER_MAIN_GROUPNAME_1' => 'Marketing (inkl. Datenweitergabe in die USA)',
	'NET_COOKIE_MANAGER_MAIN_GROUPNAME_2' => 'Essentiell',
	'NET_COOKIE_MANAGER_POPUP_HEADLINE' => 'Privatsphäre Einstellungen',
	'NET_COOKIE_NOTE_CLOSE' => 'Akzeptieren',
	'NET_COOKIE_MANAGER_MORE_INFO' => 'mehr Informationen',
	'NET_COOKIE_NOTE_ADJUST_SETTINGS' => 'Einstellungen',
	'NET_COOKIE_NOTE_HIDE_SETTINGS' => 'Zur&uuml;ck',
	'NET_COOKIE_NOTE_WELCOMETEXT' => 'Wir nutzen Cookies auf unserer Website, um Ihnen eine bestmögliche Nutzererfahrung zu bieten. Einige der Cookies sind essenziell,< um Ihre Eingaben für die von uns bereitgestellten Funktionalitäten – insbesondere einen erfolgreichen Kaufabschluss – sicherzustellen (Session Cookies). Andere helfen uns, Ihre Nutzererfahrung mit dieser Website zu verbessern. Bitte beachten Sie, dass wir auch Cookies von US-amerikanischen Firmen zur Verfügung stellen – konkret Google Analytics – und dass damit eine Drittlandübermittlung von Daten außerhalb der EU stattfinden kann. Es besteht insbesondere das Risiko, dass Ihre Daten dem Zugriff durch US-Behörden zu Kontroll- und Überwachungszwecken unterliegen und dagegen keine wirksamen Rechtsbehelfe zur Verfügung stehen. Wünschen Sie dies nicht, dann akzeptieren Sie nur die in den Einstellungen angeführten essentiellen Cookies. Alle Ihre Einwilligungen können Sie jederzeit mit Wirkung für die Zukunft widerrufen.',
	'LINKS' => 'Modellbauwerkstätten'

);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE"}]
*/
