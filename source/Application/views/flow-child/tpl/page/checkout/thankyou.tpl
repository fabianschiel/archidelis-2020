[{capture append="oxidBlock_content"}]

    [{* Google E-Commerce Tracking - nur wenn Shop im Produktivmodus! *}]
    [{if $oxcmp_shop->oxshops__oxproductive->value == 1}]

    [{assign var="_gtmOrder" value=$oView->getOrder()}]
    [{assign var="_gtmBasket" value=$oView->getBasket()}]
    [{assign var="_gtmArticles" value=$_gtmBasket->getBasketArticles()}]

    <script>
        window.dataLayer = window.dataLayer || [];
        dataLayer.push({
            transactionId: "[{$_gtmOrder->oxorder__oxordernr->value}]",
            transactionAffiliation: "[{$oxcmp_shop->oxshops__oxname->value}]",
            transactionTotal: [{$_gtmOrder->oxorder__oxtotalordersum->value}],
            transactionTax: [{math equation="x+y" x=$_gtmOrder->oxorder__oxartvatprice1->value y=$_gtmOrder->oxorder__oxartvatprice2->value }],
            transactionShipping: [{$_gtmOrder->oxorder__oxdelcost->value}],
            transactionProducts:[
                [{foreach key="_index" from=$_gtmBasket->getContents() item="_gtmBasketitem" name="gtmTransactionProducts"}]
                    [{assign var="_price" value=$_gtmBasketitem->getPrice()}]
                {
                    sku: "[{$_gtmArticles[$_index]->oxarticles__oxartnum->value}]",
                    name: "[{$_gtmArticles[$_index]->oxarticles__oxtitle->value}]",
                    price: "[{$_price->getPrice()}]",
                    quantity: "[{$_gtmBasketitem->getAmount()}]"
                }[{if !$smarty.foreach.gtmTransactionProducts.last}],[{/if}]
                    [{/foreach}]
            ]
        });
        console.log(dataLayer);
    </script>
    [{/if}]


    [{* ordering steps *}]
    [{include file="page/checkout/inc/steps.tpl" active=5}]

    [{block name="checkout_thankyou_main"}]
        [{assign var="order" value=$oView->getOrder()}]
        [{assign var="basket" value=$oView->getBasket()}]

        <div id="thankyouPage">
            <h3 class="blockHead">[{oxmultilang ident="THANK_YOU"}]</h3>

            [{block name="checkout_thankyou_info"}]
                [{oxmultilang ident="THANK_YOU_FOR_ORDER"}] [{$oxcmp_shop->oxshops__oxname->value}]. <br>
                [{oxmultilang ident="REGISTERED_YOUR_ORDER" args=$order->oxorder__oxordernr->value}] <br>
                [{if !$oView->getMailError()}]
                    [{oxmultilang ident="MESSAGE_YOU_RECEIVED_ORDER_CONFIRM"}]<br>
                [{else}]<br>
                    [{oxmultilang ident="MESSAGE_CONFIRMATION_NOT_SUCCEED"}]<br>
                [{/if}]
                <br>
                [{oxmultilang ident="MESSAGE_WE_WILL_INFORM_YOU"}]<br><br>
            [{/block}]

            [{block name="checkout_thankyou_proceed"}]
                [{oxmultilang ident="YOU_CAN_GO"}]
                <a id="backToShop" rel="nofollow" href="[{oxgetseourl ident=$oViewConf->getHomeLink()}]" class="link">[{oxmultilang ident="BACK_TO_START_PAGE"}]</a>
                [{if $oxcmp_user->oxuser__oxpassword->value}]
                    [{oxmultilang ident="OR"}]
                    <a id="orderHistory" rel="nofollow" href="[{oxgetseourl ident=$oViewConf->getSelfLink()|cat:"cl=account_order"}]" class="link">[{oxmultilang ident="CHECK_YOUR_ORDER_HISTORY"}]</a>.
                [{/if}]
            [{/block}]

            [{block name="checkout_thankyou_partners"}]
            [{/block}]

            [{if $oView->getAlsoBoughtTheseProducts()}]
                <br><br>
                <h1 class="page-header">[{oxmultilang ident="WHO_BOUGHT_ALSO_BOUGHT"}]</h1>
                [{include file="widget/product/list.tpl" type=$oView->getListDisplayType() listId="alsoBoughtThankyou" products=$oView->getAlsoBoughtTheseProducts() blDisableToCart=true}]
            [{/if}]
        </div>
    [{/block}]
    [{insert name="oxid_tracker" title=$template_title}]
[{/capture}]
[{include file="layout/page.tpl"}]
