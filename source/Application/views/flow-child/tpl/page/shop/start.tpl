[{capture append="oxidBlock_content"}]
    [{assign var="oConfig" value=$oViewConf->getConfig()}]
    [{assign var='rsslinks' value=$oView->getRssLinks()}]
    [{assign var="blFullwidth" value=$oViewConf->getViewThemeParam('blFullwidthLayout')}]
    [{oxscript include="js/pages/start.min.js"}]

    [{block name="start_welcome_text"}]
        [{oxifcontent ident="oxstartwelcome" object="oCont"}]
            <div class="welcome-teaser">[{$oCont->oxcontents__oxcontent->value}]</div>
        [{/oxifcontent}]
    [{/block}]

    [{if $oxcmp_news|count }]
    <div class="page-header">
        <h2 class="sectionHead clear" style="margin-bottom:10px; text-align: center; font-weight: 200">
            [{ oxmultilang ident="LATESTINFO" }]
        </h2>
    </div>
    <div id="phnewsbox" class="phnewsbox" [{*style="height: 200px;margin-bottom:10px;overflow:hidden; "*}]>
        <div style="padding-top:5px;">
            <a name="phnews"></a>
            [{foreach from=$oxcmp_news item=_oNewsItem name=_sNewsList }]

            [{oxeval var=$_oNewsItem->oxnews__oxlongdesc assign='_sNewsItem' force=1}]
            [{ $_sNewsItem }]<br>


            [{/foreach}]
        </div>
    </div>
    <hr>
    [{/if}]

    [{assign var="oTopArticles" value=$oView->getTop5ArticleList()}]

    [{block name="start_bargain_articles"}]
        [{assign var="oBargainArticles" value=$oView->getBargainArticleList()}]
        [{if $oBargainArticles && $oBargainArticles->count()}]
            [{include file="widget/product/list.tpl" type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') head="START_BARGAIN_HEADER"|oxmultilangassign subhead="START_BARGAIN_SUBHEADER"|oxmultilangassign listId="bargainItems" products=$oBargainArticles rsslink=$rsslinks.bargainArticles rssId="rssBargainProducts" showMainLink=true iProductsPerLine=4}]
        [{/if}]
    [{/block}]

    [{block name="start_newest_articles"}]
        [{assign var="oNewestArticles" value=$oView->getNewestArticles()}]
        [{if $oNewestArticles && $oNewestArticles->count()}]
            [{include file="widget/product/list.tpl" type=$oViewConf->getViewThemeParam('sStartPageListDisplayType') head="START_NEWEST_HEADER"|oxmultilangassign listId="newItems" products=$oNewestArticles rsslink=$rsslinks.newestArticles rssId="rssNewestProducts" showMainLink=true iProductsPerLine=4}]
        [{/if}]
    [{/block}]

    [{if $oNewestArticles && $oNewestArticles->count() && $oTopArticles && $oTopArticles->count()}]
        <div class="row">
            [{if $blFullwidth}]
                <div class="col-xs-12"><hr></div>
            [{else}]
                <hr>
            [{/if}]
        </div>
    [{/if}]

    [{block name="start_top_articles"}]
        [{if $oTopArticles && $oTopArticles->count()}]
            [{include file="widget/product/list.tpl" type="infogrid" head="START_TOP_PRODUCTS_HEADER"|oxmultilangassign:$oTopArticles->count() listId="topBox" products=$oTopArticles rsslink=$rsslinks.topArticles rssId="rssTopProducts" showMainLink=true iProductsPerLine=2}]
        [{/if}]
    [{/block}]

    [{block name="start_categories"}]
        [{include file="page/shop/inc/category_list.tpl" tree=$oxcmp_categories act=$oxcmp_categories->getClickCat() class="tree"}]
    [{/block}]

    [{insert name="oxid_tracker"}]
[{/capture}]


[{include file="layout/page.tpl"}]
