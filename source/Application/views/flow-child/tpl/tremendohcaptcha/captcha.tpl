[{assign var="oConf" value=$oViewConf->getConfig()}]
[{if $oConf->getConfigParam('tremendo_hcaptcha_sitekey')}]
    [{if $oViewConf->getTopActiveClassName() == "register"}]
    <div class="form-group">
        <label class="control-label col-lg-3 req"></label>
        <div class="col-lg-9 h-captcha" id="tremendo_hcaptcha"></div>
    </div>
    [{else}]
    <div class="h-captcha" id="tremendo_hcaptcha"></div>
    [{/if}]

[{/if}]
[{if $oConf->getConfigParam('tremendo_hcaptcha_size') == 'invisible' && $oConf->getConfigParam('tremendo_hcaptcha_privacy_terms')}]
    <p class="tremendo_hcaptcha_privacy_terms">[{oxmultilang ident="TREMENDO_HCAPTCHA_PRIVACY_TERMS"}]</p>
[{/if}]