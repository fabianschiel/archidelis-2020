<select name="[{$name}]"
        [{if $class}]class="[{$class}]"[{/if}]
        [{if $id}]id="[{$id}]"[{/if}]
        [{if $required}]required="required"[{/if}]>
    <option value="" [{if empty($value)}]SELECTED[{/if}]>[{oxmultilang ident="D1_CF_DD_CONTACT_SELECT_SALUTATION"}]</option>
    <option value="MRS" [{if $value|lower  == "mrs" or $value2|lower == "mrs"}]SELECTED[{/if}]>[{oxmultilang ident="D1_CF_MRS"}]</option>
    <option value="MR"  [{if $value|lower  == "mr"  or $value2|lower == "mr"}]SELECTED[{/if}]>[{oxmultilang ident="D1_CF_MR" }]</option>
</select>