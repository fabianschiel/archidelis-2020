<?php
/**
 * This file is part of OXID eSales Flow theme.
 *
 * OXID eSales Flow theme is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * OXID eSales Flow theme is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with OXID eSales Flow theme.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @link      http://www.oxid-esales.com
 * @copyright (C) OXID eSales AG 2003-2016
 */

$sLangName = "English";

$aLang = array(
    'charset'                                               => 'UTF-8',

    // Global
    'DD_SORT_DESC'                                          => 'descending',
    'DD_SORT_ASC'                                           => 'ascending',
    'DD_CLOSE_MODAL'                                        => 'close',
    'DD_DEMO_ADMIN_TOOL'                                    => 'Open admin interface',
    'DD_DELETE'                                             => 'Delete',

	'DD_NAVIGATION_MORE'                                    => 'More',

    // Form validation
    'DD_FORM_VALIDATION_VALIDEMAIL'                         => 'Please enter a valid email address.',
    'DD_FORM_VALIDATION_PASSWORDAGAIN'                      => 'Passwords do not match.',
    'DD_FORM_VALIDATION_NUMBER'                             => 'Please enter a number.',
    'DD_FORM_VALIDATION_INTEGER'                            => 'There are no decimal places allowed.',
    'DD_FORM_VALIDATION_POSITIVENUMBER'                     => 'Please enter a positive number.',
    'DD_FORM_VALIDATION_NEGATIVENUMBER'                     => 'Please enter a negative number.',
    'DD_FORM_VALIDATION_REQUIRED'                           => 'Please specify a value for this required field.',
    'DD_FORM_VALIDATION_CHECKONE'                           => 'Please select at least one option.',

    // Header
    'SEARCH_TITLE'                                          => 'Enter a search term...',
    'SEARCH_SUBMIT'                                         => 'Search',
    'COOKIE_NOTE_CLOSE'                                     => 'Close',

    // Sidebar
    'DD_SIDEBAR_CATEGORYTREE'                               => 'Categories',

    // Footer
    'FOOTER_NEWSLETTER_INFO'                                => 'Get informed about the latest products and offers per email.',

    // Home page
    'MANUFACTURERSLIDER_SUBHEAD'                            => 'We present our carefully selected brands, whose products can be found in our shop.',
    'START_BARGAIN_HEADER'                                  => 'Week\'s Special',
    'START_NEWEST_HEADER'                                   => 'Just arrived',
    'START_TOP_PRODUCTS_HEADER'                             => 'Top seller',
    'START_BARGAIN_SUBHEADER'                               => 'Save money with our current bargains!',
    'START_NEWEST_SUBHEADER'                                => 'Fresh as it gets. One minute in the box and now already in store.',
    'START_TOP_PRODUCTS_SUBHEADER'                          => 'Only %s products, but the best we can offer you.',

    // Contact form
    'DD_CONTACT_PAGE_HEADING'                               => 'Contact us!',
    'DD_CONTACT_FORM_HEADING'                               => 'Contact',
    'DD_CONTACT_ADDRESS_HEADING'                            => 'Address',
    'DD_CONTACT_THANKYOU1'                                  => "Thank you.",
    'DD_CONTACT_THANKYOU2'                                  => " appreciates your comments.",
    'DD_CONTACT_SELECT_SALUTATION'                          => 'Please choose ...',

    // Link list
    'DD_LINKS_NO_ENTRIES'                                   => 'Unfortunately, there are no links available.',

    // 404 page
    'DD_ERR_404_START_TEXT'                                 => 'You may find the information you want from our home page:',
    'DD_ERR_404_START_BUTTON'                               => 'Go to home page',
    'DD_ERR_404_CONTACT_TEXT'                               => 'May we assist you?<br>Feel free to call us or write an email:',
    'DD_ERR_404_CONTACT_BUTTON'                             => 'to the contact page',

    // Login
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_TITLE'                   => 'Open account',
    'DD_LOGIN_ACCOUNT_PANEL_CREATE_BODY'                    => 'By creating an account with our store, you will be guided through the checkout process faster. In addition, you can store multiple shipping addresses and track orders in your account.',
    'DD_LOGIN_ACCOUNT_PANEL_LOGIN_TITLE'                    => 'Login',

    // Billing address
    'DD_USER_BILLING_LABEL_STATE'                           => 'State:',
    'DD_USER_SHIPPING_LABEL_STATE'                          => 'State:',
    'DD_USER_SHIPPING_SELECT_ADDRESS'                       => 'select',
    'DD_USER_SHIPPING_ADD_DELIVERY_ADDRESS'                 => 'add delivery address',
    'DD_DELETE_SHIPPING_ADDRESS'                            => 'Delete shipping address',

    // Order history
    'DD_ORDER_ORDERDATE'                                    => 'Date:',

    // List views
    'DD_LISTLOCATOR_FILTER_ATTRIBUTES'                      => 'Filter:',
    'DD_LIST_SHOW_MORE'                                     => 'View products...',

    // Recommendation list
    'DD_RECOMMENDATION_EDIT_BACK_TO_LIST'                   => 'back to overview',

    // Downloads
    'DD_DOWNLOADS_DOWNLOAD_TOOLTIP'                         => 'downloaded',
    'DD_FILE_ATTRIBUTES_FILESIZE'                           => 'File size:',
    'DD_FILE_ATTRIBUTES_OCLOCK'                             => 'o\'clock',
    'DD_FILE_ATTRIBUTES_FILENAME'                           => 'File name:',

    // Details page
    'BACK_TO_OVERVIEW'                                      => 'to overview',
    'OF'                                                    => 'of',
    'DD_PRODUCTMAIN_STOCKSTATUS'                            => 'Stock',
    'DD_RATING_CUSTOMERRATINGS'                             => 'Customer reviews',
    'PAGE_DETAILS_CUSTOMERS_ALSO_BOUGHT_SUBHEADER'          => 'Customers who bought this item also bought one of the following products.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_ACCESSORIES_SUBHEADER' => 'The following products fit well to this product.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_SIMILAR_SUBHEADER'     => 'Take a look at our similar products.',
    'WIDGET_PRODUCT_RELATED_PRODUCTS_CROSSSELING_SUBHEADER' => 'Customers who viewed this product also viewed the following products.',
    'DETAILS_VPE_MESSAGE_1'                                 => "This product can only be ordered in packaging units of ",
    'DETAILS_VPE_MESSAGE_2'                                 => "",
    'HAVE_YOU_SEEN'                                               => 'Matching Products:',

    // Modal basket
    'DD_MINIBASKET_MODAL_TABLE_TITLE'                       => 'Article',
    'DD_MINIBASKET_MODAL_TABLE_PRICE'                       => 'Total',
    'DD_MINIBASKET_CONTINUE_SHOPPING'                       => 'continue shopping',

    // Checkout
    'DD_BASKET_BACK_TO_SHOP'                                => 'back to shop',

    // E-Mails
    'DD_ORDER_CUST_HEADING'                                 => 'Order',
    'DD_FOOTER_FOLLOW_US'                                   => 'Follow us:',
    'DD_FOOTER_CONTACT_INFO'                                => 'Contact:',
    'DD_FORGOT_PASSWORD_HEADING'                            => 'Forgot password',
    'DD_INVITE_HEADING'                                     => 'Article recommendation',
    'DD_INVITE_LINK'                                        => 'Link',
    'DD_NEWSLETTER_OPTIN_HEADING'                           => 'Your newsletter subscription',
    'DD_ORDERSHIPPED_HEADING'                               => 'Delivery confirmation - Order',
    'DD_OWNER_REMINDER_HEADING'                             => 'Low stock',
    'DD_PRICEALARM_HEADING'                                 => 'Pricealarm',
    'DD_REGISTER_HEADING'                                   => 'Your registration',
    'DD_DOWNLOADLINKS_HEADING'                              => 'Your download links - Order',
    'DD_SUGGEST_HEADING'                                    => 'Article recommendation',
    'DD_WISHLIST_HEADING'                                   => 'Wishlist',

    'DD_ROLES_BEMAIN_UIROOTHEADER'                          => 'Menu',

    'DD_DELETE_MY_ACCOUNT_WARNING'                          => 'This action cannot be undone. This will permanently delete your personal data.',
    'DD_DELETE_MY_ACCOUNT'                                  => 'Delete account',
    'DD_DELETE_MY_ACCOUNT_CONFIRMATION_QUESTION'            => 'Are you sure you want to delete your account?',
    'DD_DELETE_MY_ACCOUNT_CANCEL'                           => 'Cancel',
    'DD_DELETE_MY_ACCOUNT_SUCCESS'                          => 'The account has been deleted',
    'DD_DELETE_MY_ACCOUNT_ERROR'                            => 'The account could not have been deleted',

    // Account -> My product reviews
    'DD_DELETE_REVIEW_AND_RATING'                           => 'Delete review and star rating',
    'DD_REVIEWS_NOT_AVAILABLE'                              => 'No reviews available',
    'DD_DELETE_REVIEW_CONFIRMATION_QUESTION'                => 'Are you sure you want to delete the review?',

    'DD_CATEGORY_RESET_BUTTON'                              => 'Reset',


    'BUY-AT-ARCH'                                           => 'Archidelis',

    'SPERRGUT'                                           => 'For extra-large packages (bulky goods) we have to charge a fee - see <a href="/Versandkosten-Zahlung-und-Lieferung?lang=1"> SHIPPING AND CHARGES</a>',
    'LATESTINFO'                                                => "Latest Info",
    'D1Netto'       =>                  'net amount',
    'D1NettoRd'       =>                  'net rd.',

    // Owner E-Mail Custom Fields EN
    'D1_Email_BILLING_ADDRESS'                                          => 'Billing address',
    'D1_Email_VAT_ID_NUMBER'                                               => 'VAT ID',
    'D1_Email_PHONE'                                                       => 'Phone',
    'D1_Email_SHIPPING_ADDRESS'                                          => 'Shipping address',
    'D1_Email_PRODUCT'                                          => 'Product',
    'D1_Email_UNIT_PRICE'                                          => 'Unit price',
    'D1_Email_QUANTITY'                                          => 'Quantity',
    'D1_Email_VAT'                                         		 => 'VAT',
    'D1_Email_TOTAL'                                          => 'Total',
    'D1_Email_PRODUCT_NO'                                          => 'Item #',
    'D1_Email_DISCOUNT'                                          => 'Discount',
    'D1_Email_USED_COUPONS'                                          => 'The following coupons are used',
    'D1_Email_REBATE'                                          => 'Discount',
    'D1_Email_TOTAL_NET'                                          => 'Total products (excl. tax)',
    'D1_Email_VAT_PLUS_PERCENT_AMOUNT'                            => 'plus %s%% tax, amount',
    'D1_Email_TOTAL_GROSS'                                          => 'Total products (incl. tax)',
    'D1_Email_SURCHARGE'                                          => 'Surcharge',
    'D1_Email_COUPON'                                          => 'Coupon',
    'D1_Email_SHIPPING_NET'                                          => 'Shipping (excl. tax)',
    'D1_Email_BASKET_TOTAL_PLUS_PROPORTIONAL_VAT'                     => 'plus tax (calculated proportionally)',
    'D1_Email_SHIPPING_COST'                                          => 'Shipping costs',
    'D1_Email_PLUS_VAT'                                          => 'plus tax',
    'D1_Email_GRAND_TOTAL'                                          => 'Grand total',
    'D1_Email_MESSAGE'                                          => 'Message',
    'D1_Email_PAYMENT_INFORMATION'                                          => 'Payment information',
    'D1_Email_PAYMENT_METHOD'                                          => 'Payment method',
    'D1_Email_DEDUCTION'                                                   => 'Deduction',
    'D1_Email_EMAIL_ADDRESS'                                          => 'Your e-mail address',
    'D1_Email_SELECTED_SHIPPING_CARRIER'                                          => 'Selected shipping carrier is',
    'D1_Email_PRODUCT_REVIEW'                                              => 'Product review',
    'D1_Email_WHAT_I_WANTED_TO_SAY'                                        => 'Drop us a message',
    'D1_Email_MY_DOWNLOADS_DESC'                                           => 'Download your ordered files here.',
    'D1_Email_DOWNLOADS_PAYMENT_PENDING'                                   => 'Payment of the order is not yet complete.',
    'D1_Email_BANK'                                                        => 'Bank',
    'D1_Email_BANK_DETAILS'                                                => 'Bank details',
    'D1_Email_BANK_ACCOUNT_HOLDER'                                         => 'Account holder',
    'D1_Email_BANK_ACCOUNT_NUMBER'                                         => 'Account no. or IBAN',
    'D1_Email_BANK_CODE'                                                   => 'Bank code or BIC',
    'D1_Email_BIC'                                                         => 'BIC',
    'D1_Email_IBAN'                                                        => 'IBAN',
    'D1_Email_DD_FOOTER_FOLLOW_US'                                   => 'Follow us:',
    'D1_Email_DD_FOOTER_CONTACT_INFO'                                => 'Contact:',

    //Footer Custom
    'SERVICES' => 'SERVICE ',
    'CONTACT' => 'Contact',
    'HELP' => 'Help',
    'LINKS' => 'Links',
    'CART' => 'Cart',
    'ACCOUNT' => 'Account',
    'WISH_LIST' => 'Wish list',
    'MY_GIFT_REGISTRY' => 'My gift registry',
    'PUBLIC_GIFT_REGISTRIES' => 'Public gift registry',
    'footer_information' => 'INFORMATION',
    'INFORMATION' => 'INFORMATION',
    'NEWSLETTER' => 'Newsletter',
    'NEWS' => 'News',
    'footer_categories' => 'CATEGORIES',
    'CATEGORIES' => 'CATEGORIES',
    'FOOTER_NEWSLETTER_INFO' => 'Get informed about the latest products and offers per email.',
    'SUBSCRIBE' => 'Subscribe',

    //Custom Fields for Contact Form
    'D1_CF_TITLE' => 'Salutation',
    'D1_CF_FIRST_NAME' => 'First name',
    'D1_CF_LAST_NAME' => 'Last name',
    'D1_CF_EMAIL' => 'E-mail',
    'D1_CF_SUBJECT' => 'Subject',
    'D1_CF_MESSAGE' => 'Message',
    'D1_CF_COMPLETE_MARKED_FIELDS' => 'Please fill in all mandatory fields labeled in bold.',
    'D1_CF_SEND' => 'Send',
    'D1_CF_DD_CONTACT_SELECT_SALUTATION'                          => 'Please choose ...',
    'D1_CF_MRS'                          => 'Mrs',
    'D1_CF_MR'                          => 'Mr',


    'DD_CONTACT_THANKYOU1'                                  => "Thank you.",
    'DD_CONTACT_THANKYOU2'                                  => " appreciates your comments.",

	'NET_COOKIE_MANAGER_MAIN_GROUPNAME_0'           => 'Functional',
	'NET_COOKIE_MANAGER_MAIN_GROUPNAME_1'           => 'Marketing (data transmission to USA included)',
	'NET_COOKIE_MANAGER_MAIN_GROUPNAME_2'           => 'Essential',
	'NET_COOKIE_MANAGER_POPUP_HEADLINE'             => 'Privacy settings',
	'NET_COOKIE_NOTE_CLOSE'                         => 'Accept',
	'NET_COOKIE_MANAGER_MORE_INFO'                  => 'more information',
	'NET_COOKIE_NOTE_ADJUST_SETTINGS'               => 'Settings',
	'NET_COOKIE_NOTE_HIDE_SETTINGS'                 => 'Back',
	'NET_COOKIE_NOTE_WELCOMETEXT'                   => 'We use cookies on our website to offer you the best possible user experience. Some of the cookies are essential< to ensure your input for the functionalities provided by us - in particular a successful purchase - (session cookies). Others help us to improve your user experience on this website. Please note that we also provide cookies from US companies - specifically Google Analytics - and that data may be transferred to a third country outside the EU. In particular, there is a risk that your data may be accessed by US authorities for control and monitoring purposes and that no effective legal remedies are available. If you do not want this, then only accept the essential cookies listed in the settings. You can revoke all your consents at any time with effect for the future.',
	'LINKS' => 'Model maker'

);

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE"}]
*/
