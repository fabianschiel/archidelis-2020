<?php
$aTheme = array(
    'id' => 'flowchild-archidelis',
    'title' => 'Flow Child Theme - Archidelis',
    'description' => 'Das ist das Child-Theme fuer den Archidelis Shop',
    //TODO Screenshoot 'thumbnail' => 'theme.jpg',
    'version' => '1.0',
    'author' => 'Fabian Schiel',
    //Define the following variables for the custom child themes:
    'parentTheme' => 'flow',
    'parentVersions' => array('3.3.0')
);