<?php
/**
 * Copyright © OXID eSales AG. All rights reserved.
 * See LICENSE file for license details.
 */

$sLangName  = "Deutsch";
// -------------------------------
// RESOURCE IDENTIFIER = STRING
// -------------------------------
$aLang = [

    'charset'                                  => 'UTF-8',

//Credit card
    'ORDER_OVERVIEW_KKTYPE' => 'Kreditkartentyp',
    'ORDER_OVERVIEW_KKNUMBER' => 'Kartennummer',
    'ORDER_OVERVIEW_KKMONTH' => 'Verfall-Monat',
    'ORDER_OVERVIEW_KKYEAR' => 'Verfall-Jahr',
    'ORDER_OVERVIEW_KKNAME' => 'Name auf Karte',
    'ORDER_OVERVIEW_KKPRUEF' => 'Prüfziffer',

];

/*
[{ oxmultilang ident="GENERAL_YOUWANTTODELETE" }]
*/